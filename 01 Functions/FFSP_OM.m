function [PROBLEM, x_var]=FFSP_OM(par, In, opt)

% v11b without with power directions
%% Define variables

%x_var defines the dimension of the variables with individual upper and
%lower bounds and types[C=Continuous, B=binary, I=Integer]. Variable names
%are important as they are used to define subdivisions of Matrices A and
%vectors b
x_var.subdim={};
x_var.type=[];
x_var.dim.vect=[];
x_var.dim.vect_start=[];
x_var.dim.vect_end=[];
x_var.dim.total=0;
x_var.names=[];
x_var.ub=[];
x_var.lb=[];
big_M=1000000;

% x_var is the variable vector and CreateVariable creates a new variable
% to the variable vector with its parameters
% createVariable( variable vector, variable name,       variable dimension,               {'number of dimensions' 'dimension 1' 'dimension 2' 'dimension3' 'dimension4'},       'variable type',    variable lower bound,  variable upper bound);
% all subdimensions need to be specified by a string such as 'T' for time and value for the
% length of this subdimension has to be defined in the parameter vector as
% 'par.T=10'
x_var   =   createVariable(x_var, 'S_M_prod',       par.M * par.I * par.T,               {'3' 'M' 'I' 'T' ''},       'B',    0,  1);
x_var   =   createVariable(x_var, 'S_M_setup',      par.M * par.I * par.I * par.T,       {'4' 'M' 'I' 'I' 'T'},      'B',    0,  1);
x_var   =   createVariable(x_var, 'S_M_idle',       par.M * par.I * par.T,               {'3' 'M' 'I' 'T' ''},       'B',    0,  1);
x_var   =   createVariable(x_var, 'S_M_loading',    par.M * par.K * par.J * par.T,       {'4' 'M' 'K' 'J' 'T' },     'B',    0,  1);

x_var   =   createVariable(x_var, 'S_M_sup',        par.M * par.I * par.T,               {'3' 'M' 'I' 'T' ''},       'B',    0,  1);
x_var   =   createVariable(x_var, 'S_M_sdn',        par.M * par.I * par.T,               {'3' 'M' 'I' 'T' ''},       'B',    0,  1);
x_var   =   createVariable(x_var, 'S_M_off',        par.M * par.T,                       {'2' 'M' 'T' ''  ''},       'B',    0,  1);

x_var   =   createVariable(x_var, 'I_buffer',       par.K * par.J * par.T,               {'3' 'K' 'J' 'T' ''},       'I',    0,  big_M);
x_var   =   createVariable(x_var, 'I_fin_prod',     par.K * par.T,                       {'2' 'K' 'T' '' ''},       'I',    0,  big_M);

x_var   =   createVariable(x_var, 'I_MS',           par.K,                               {'1' 'K' '' '' ''},       'C',    0,  big_M);

x_var   =   createVariable(x_var, 'S_job_completion', par.K * par.T,                     {'2' 'K' 'T' ''  ''},       'B',    0,  1);
x_var   =   createVariable(x_var, 'S_lastjob_completion', par.T,                         {'1' 'T' '' ''  ''},       'B',    0,  1);


x_var   =   createVariable(x_var, 'E_cons',         par.M * par.T,                       {'2' 'M' 'T' '' ''},        'C',    0,  big_M);
x_var   =   createVariable(x_var, 'E_deltaFlex',    par.T,                               {'1' 'T' '' '' ''},        'C',    0,  big_M);


x_var   =   createVariable(x_var, 'P_ESScrg',       par.T,            {'1' 'T' '' '' ''},        'C',    0,  par.ESS.P_c_max);
x_var   =   createVariable(x_var, 'P_ESSdcrg',      par.T,            {'1' 'T' '' '' ''},        'C',    0,  par.ESS.P_d_max);
x_var   =   createVariable(x_var, 'E_ESSstore',     par.T_1,          {'1' 'T_1' '' '' ''},      'C',    par.ESS.E_min,  par.ESS.E_max);
x_var   =   createVariable(x_var, 'S_ESScrg',       par.T,            {'1' 'T' '' '' ''},        'B',    0,  1);
x_var   =   createVariable(x_var, 'S_ESSdcrg',      par.T,            {'1' 'T' '' '' ''},        'B',    0,  1);
x_var   =   createVariable(x_var, 'S_ESSidle',      par.T,            {'1' 'T' '' '' ''},        'B',    0,  1);


x_var   =   createVariable(x_var, 'E_Pur',          par.T,            {'1' 'T' '' '' ''},       'C',    0,  big_M); %-big_M * opt.INCL_Pgrid_negVariable darf negativ werden falls R?ckspeisung zul?ssig
x_var   =   createVariable(x_var, 'E_Grid_sup',       par.T,            {'1' 'T' '' '' ''},       'C',    0,  big_M); %-big_M * opt.INCL_Pgrid_negVariable darf negativ werden falls R?ckspeisung zul?ssig
x_var   =   createVariable(x_var, 'S_Grid_sup',       par.T,            {'1' 'T' '' '' ''},       'B',    0,  1); 
%x_var   =   createVariable(x_var, 'E_PVcurt',       par.T,            {'1' 'T' '' '' ''},        'C',    0,  0);

% Problem ist die Problemvariable fuer CPLEX.
PROBLEM.ub=x_var.ub;
PROBLEM.lb=x_var.lb;
PROBLEM.ctype=x_var.type;

%% objective Function OF1
%% cost function depending on DAM prices
[f_sub]=createMatrix( 1 , x_var);
w_c=par.obj_weights.w_Cost;
w_t=par.obj_weights.w_MS;
for t=1:par.T
f_sub.E_Pur(1, t)= In.DAM.p_t(t)/1000 * w_c;
f_sub.E_deltaFlex( 1, t) = big_M;
end
for k=1:par.K
f_sub.I_MS(1, k)= w_t;
%f_sub.S_job_completion(1, (k-1)*par.T + t)=w_t;
end


PROBLEM.f = concatenateMatrix(x_var,f_sub);
clearvars f_sub



%% objective Function OF1sub
%% cost function depending on DAM prices
[A_eq_OFMS_sub, b_eq_OFMS]=createMatrix( par.K , x_var);
c=1;
for k=1:par.K
A_eq_OFMS_sub.I_MS(c, k)= -1;
for t=1:par.T
    A_eq_OFMS_sub.S_job_completion(c, (k-1) * par.T + t)= (t-par.T_S) * par.delta_T;
end
c=c+1;
end


PROBLEM = concatCPLEXinput(PROBLEM, A_eq_OFMS_sub, b_eq_OFMS, x_var, 'eq');
clearvars -except PROBLEM x_var par In big_M opt

%% Receding horizon technique

if opt.RecedingHorizon==1
dim_RH = par.T_RH * (5 + par.M * (1 + par.I * ( 4 +par.I)));% + par.K * par.J));
[A_eq_RH_sub, b_eq_RH]=createMatrix( dim_RH , x_var);
c=1;
for t=1:par.T_RH
   for m=1:par.M
       A_eq_RH_sub.S_M_off(c,(m-1)*par.T + t)=1;
       b_eq_RH(c,1)=round(In.RecedingHorizon.S_M_off_MT(m,t));
       c=c+1;
       for i=1:par.I
           A_eq_RH_sub.S_M_prod(c,(m-1)*par.I*par.T + (i-1)*par.T + t)=1;
           b_eq_RH(c,1)=round(In.RecedingHorizon.S_M_prod_MIT(m,i,t));
           c=c+1;
           for  i2=1:par.I
               A_eq_RH_sub.S_M_setup(c,(m-1)*par.I*par.I*par.T + (i-1)*par.I*par.T + (i2-1)*par.T + t)=1;
               b_eq_RH(c,1)=round(In.RecedingHorizon.S_M_setup_MIIT(m,i,i2,t));
               c=c+1;
           end
           A_eq_RH_sub.S_M_idle(c,(m-1)*par.I*par.T + (i-1)*par.T + t)=1;
           b_eq_RH(c,1)=round(In.RecedingHorizon.S_M_idle_MIT(m,i,t));
           c=c+1;
           A_eq_RH_sub.S_M_sup(c,(m-1)*par.I*par.T + (i-1)*par.T + t)=1;
           b_eq_RH(c,1)=round(In.RecedingHorizon.S_M_sup_MIT(m,i,t));
           c=c+1;
           A_eq_RH_sub.S_M_sdn(c,(m-1)*par.I*par.T + (i-1)*par.T + t)=1;
           b_eq_RH(c,1)=round(In.RecedingHorizon.S_M_sdn_MIT(m,i,t));
           c=c+1;
       end


%            for k=1:par.K
%                for j=1:par.J
%                    A_eq_RH_sub.S_M_loading(c,(m-1) * par.K * par.J * par.T + (k-1) * par.J * par.T + (j-1) * par.T + t)=-1;
%                    b_eq_RH(c,1)=round(In.RecedingHorizon.S_M_loading_MKJT(m,k,j,t));
%                    c=c+1;
%                end
%            end
   end

   A_eq_RH_sub.P_ESScrg (c, t)=1;
   b_eq_RH(c,1)=In.RecedingHorizon.P_ESScrg_T(t);
   c=c+1;
   A_eq_RH_sub.P_ESSdcrg (c, t)=1;
   b_eq_RH(c,1)=In.RecedingHorizon.P_ESSdcrg_T(t);
   c=c+1;
   %A_eq_RH_sub.E_ESSstore (c, t)=1;
   %b_eq_RH(c,1)=In.RecedingHorizon.E_ESSstore_T_1(t);
   %c=c+1;
%    A_eq_RH_sub.S_ESScrg (c, t)=1;
%    b_eq_RH(c,1)=round(In.RecedingHorizon.S_ESScrg_T(t));
%    c=c+1;
%    A_eq_RH_sub.S_ESSdcrg (c, t)=1;
%    b_eq_RH(c,1)=round(In.RecedingHorizon.S_ESSdcrg_T(t));
%    c=c+1;
%    A_eq_RH_sub.S_ESSidle (c, t)=1;
%    b_eq_RH(c,1)=round(In.RecedingHorizon.S_ESSidle_T(t));
%    c=c+1;
end
PROBLEM = concatCPLEXinput(PROBLEM, A_eq_RH_sub, b_eq_RH, x_var, 'eq');
clearvars -except PROBLEM x_var par In big_M opt

end

%% Revert Horizon
if opt.RevertHorizon==1
dim_RVH = (par.T-par.T_RVH+1) * 2;
[A_eq_RVH_sub, b_eq_RVH]=createMatrix( dim_RVH , x_var);
c=1;
for t=par.T_RVH:par.T 
   A_eq_RVH_sub.P_ESScrg (c, t)=1;
   b_eq_RVH(c,1)=In.RecedingHorizon.P_ESScrg_T(t);
   c=c+1;
   A_eq_RVH_sub.P_ESSdcrg (c, t)=1;
   b_eq_RVH(c,1)=In.RecedingHorizon.P_ESSdcrg_T(t);
   c=c+1;
end
PROBLEM = concatCPLEXinput(PROBLEM, A_eq_RVH_sub, b_eq_RVH, x_var, 'eq');
clearvars -except PROBLEM x_var par In big_M opt

end


%% PM1 Machine usage constraints PM1
[A_eq_PM1_sub, b_eq_PM1]=createMatrix( par.I * par.M * par.T , x_var);
c=1;
for m=1:par.M % Erstellung der einzelnen Matrizen durch abbilden der Gleichungen
for i=1:par.I
    for t=1:par.T
        % production state
        t_alfa=t-In.P_im(i,m);
        if t_alfa>=1
            A_eq_PM1_sub.S_M_prod( c,      (m-1) * par.I * par.T + (i-1) * par.T + t_alfa )  =1;
        end
        A_eq_PM1_sub.S_M_prod( c,       (m-1) * par.I * par.T + (i-1) * par.T + t)=-1;

        % startup mode
        t_beta=t-In.SUD_im(i,m);
        if t_beta>=1
            A_eq_PM1_sub.S_M_sup( c,      (m-1) * par.I * par.T + (i-1) * par.T + t_beta )  =1;
        end
        A_eq_PM1_sub.S_M_sdn( c,      (m-1) * par.I * par.T + (i-1) * par.T + t)  =-1;

        % idle mode
        if t>1
            A_eq_PM1_sub.S_M_idle( c,       (m-1) * par.I * par.T + (i-1) * par.T + t-1 )       =1;
        end
        A_eq_PM1_sub.S_M_idle( c,       (m-1) * par.I * par.T + (i-1) * par.T + t )       =-1;

        %setup mode
        for l=1:par.I
            if not(i==l)
                t_setup=t-In.A_lim(i,l,m); %hier andere Index, weil hier hingewechselt wird
                if t_setup>0
                A_eq_PM1_sub.S_M_setup( c,	(m-1) * par.I * par.I * par.T + (l-1) * par.I * par.T + (i-1) * par.T + t_setup)   =1;
                end
                A_eq_PM1_sub.S_M_setup( c,    (m-1) * par.I * par.I * par.T + (i-1) * par.I * par.T + (l-1) * par.T + t)        =-1;
            end
        end
        c=c+1; %Constraint laufvariable
    end
end
end
PROBLEM = concatCPLEXinput(PROBLEM, A_eq_PM1_sub, b_eq_PM1, x_var, 'eq');
clearvars -except PROBLEM x_var par In big_M opt

%% PM1A Machine usage constraints PM1A Start and End Times
T_PM1A=(par.T_S) + par.T-(par.T_E-1) + 1;
dim_PM1A= par.M * T_PM1A + par.M * T_PM1A + par.M * par.I * T_PM1A + par.M * par.I * (par.I-1)* T_PM1A ;
[A_eq_PM1A_sub, b_eq_PM1A]=createMatrix( dim_PM1A , x_var);
c=1;
for t=1:par.T_S
for m=1:par.M % Erstellung der einzelnen Matrizen durch abbilden der Gleichungen
    A_eq_PM1A_sub.S_M_off( c,       (m-1) * par.T + t)        =1;
    b_eq_PM1A(c,1)=1;
    c = c + 1;  
end
end  
for t=(par.T_E-1):par.T
for m=1:par.M % Erstellung der einzelnen Matrizen durch abbilden der Gleichungen
    A_eq_PM1A_sub.S_M_off( c,       (m-1) * par.T + t)        =1;
    b_eq_PM1A(c,1)=1;
    c = c + 1;
end
end
for t=1:par.T_S
for m=1:par.M % Erstellung der einzelnen Matrizen durch abbilden der Gleichungen
    for i=1:par.I
        A_eq_PM1A_sub.S_M_prod( c,       (m-1) * par.I * par.T + (i-1) * par.T + t)        =1;
        c=c+1;
    end    
end
end  
for t=(par.T_E-1):par.T
for m=1:par.M % Erstellung der einzelnen Matrizen durch abbilden der Gleichungen
    for i=1:par.I
        A_eq_PM1A_sub.S_M_prod( c,       (m-1) * par.I * par.T + (i-1) * par.T + t)        =1;
        c=c+1;
    end    
end
end
for t=1:par.T_S
for m=1:par.M % Erstellung der einzelnen Matrizen durch abbilden der Gleichungen
    A_eq_PM1A_sub.S_M_idle( c,       (m-1) * par.T + t)        =1;
    c=c+1;  
end
end  
for t=(par.T_E-1):par.T
for m=1:par.M % Erstellung der einzelnen Matrizen durch abbilden der Gleichungen
    A_eq_PM1A_sub.S_M_idle( c,       (m-1) * par.T + t)        =1;
    c=c+1;   
end
end
for t=1:par.T_S
for i=1:par.I
    for m=1:par.M
        for l=1:par.I
            if not(i==l)
              A_eq_PM1A_sub.S_M_setup( c,    (m-1) * par.I * par.I *par.T + (i-1) * par.I *par.T  + (l-1) * par.T + t)        =1;
              c=c+1;
            end
        end         
    end
end
end
for t=(par.T_E-1):par.T
for i=1:par.I
    for m=1:par.M
        for l=1:par.I
            if not(i==l)
              A_eq_PM1A_sub.S_M_setup( c,    (m-1) * par.I * par.I *par.T + (i-1) * par.I *par.T + (l-1) * par.T + t)        =1;
              c=c+1;
            end
        end         
    end
end
end

PROBLEM = concatCPLEXinput(PROBLEM, A_eq_PM1A_sub, b_eq_PM1A, x_var, 'eq');
clearvars -except PROBLEM x_var par In big_M opt

%% PM2 Machine usage constraints PM1
[A_eq_PM2_sub, b_eq_PM2]=createMatrix( par.M * par.T , x_var);
c=1;
for m=1:par.M % Erstellung der einzelnen Matrizen durch abbilden der Gleichungen
for t=1:par.T
    % production state
    for i=1:par.I
        t_beta=t-In.SUD_im(i,m);
        if t_beta>=1
            A_eq_PM2_sub.S_M_sdn( c,      (m-1) * par.I * par.T + (i-1) * par.T + t_beta )  =-1;
        end
        A_eq_PM2_sub.S_M_sup( c,       (m-1) * par.I * par.T + (i-1) * par.T + t)=1;
    end
    A_eq_PM2_sub.S_M_off( c,       (m-1) *par.T + t)=1;
    if t>1
        A_eq_PM2_sub.S_M_off( c,       (m-1) * par.T + t-1 )       =-1;
    else
        b_eq_PM2(c,1)=1;
    end
    c=c+1; %Constraint laufvariable
end
end
PROBLEM = concatCPLEXinput(PROBLEM, A_eq_PM2_sub, b_eq_PM2, x_var, 'eq');
clearvars -except PROBLEM x_var par In big_M opt


%% PP1 Buffer constraints PP1
[A_eq_PP1_sub, b_eq_PP1]=createMatrix( par.J * par.K * (par.T-1), x_var);
for k=1:par.K % Erstellung der einzelnen Matrizen durch abbilden der Gleichungen
for j=1:par.J
    for t=1:par.T-1
        % Buffergleichungen
        if t==1
            A_eq_PP1_sub.I_buffer( (k-1) * par.J * (par.T-1)  + (j-1) * (par.T-1)  + t, (k-1) * par.J *par.T + (j-1) * par.T + t)=0;
        else
            A_eq_PP1_sub.I_buffer( (k-1) * par.J * (par.T-1)  + (j-1) * (par.T-1)  + t, (k-1) * par.J *par.T + (j-1) * par.T + t)=1;
        end
        A_eq_PP1_sub.I_buffer( (k-1) * par.J * (par.T-1)  + (j-1) * (par.T-1)  + t, (k-1) * par.J *par.T + (j-1) * par.T + t + 1)=-1;

        % Loading entwicklung
        for var=1:length(In.M_J(j,:))
            m=In.M_J(j,var);
            if m>0
               A_eq_PP1_sub.S_M_loading( (k-1) * par.J * (par.T-1) + (j-1) * (par.T-1)  + t, (m-1) * par.K * par.J *par.T + (k-1) * par.J *par.T + (j-1) * par.T + t) =  -par.M_P(m,In.D.E_k(k));
            end
        end
        if j>1
            j_loading=j-1;
            for var=1:length(In.M_J(j_loading,:))
                m=In.M_J(j_loading,var);
                if m>0
                    t_loading = t-In.P_im( In.D.E_k(k),m );
                    if t_loading>0
                        A_eq_PP1_sub.S_M_loading( (k-1) * par.J * (par.T-1) + (j-1) * (par.T-1)  + t, (m-1) * par.K * par.J *par.T + (k-1) * par.J *par.T + (j_loading-1) * par.T + t_loading) =    par.M_P(m,In.D.E_k(k));
                    end
                end
            end
        end

        if j==1
        b_eq_PP1 ((k-1) * par.J * (par.T-1) + (j-1) * (par.T-1)  + t)=-In.D.r_kt(k,t);
        end
    end
end
end
PROBLEM = concatCPLEXinput(PROBLEM, A_eq_PP1_sub, b_eq_PP1, x_var, 'eq');
clearvars -except PROBLEM x_var par In big_M opt

%% PP1A Buffer constraints PP1A
[A_ineq_PP1A_sub, b_ineq_PP1A]=createMatrix( par.J * par.K * (par.T-1) , x_var);
for k=1:par.K % Erstellung der einzelnen Matrizen durch abbilden der Gleichungen
for j=1:par.J
    for t=1:par.T-1
        % Buffergleichungen
        A_ineq_PP1A_sub.I_buffer( (k-1) * par.J * (par.T-1)  + (j-1) * (par.T-1)  + t, (k-1) * par.J *par.T + (j-1) * par.T + t)=-1;

        % Loading entwicklung
        for var=1:length(In.M_J(j,:))
            m=In.M_J(j,var);
            if m>0
               A_ineq_PP1A_sub.S_M_loading( (k-1) * par.J * (par.T-1) + (j-1) * (par.T-1)  + t, (m-1) * par.K * par.J *par.T + (k-1) * par.J *par.T + (j-1) * par.T + t)=1;
            end
        end
        if j>1
            j_loading=j-1;
            for var=1:length(In.M_J(j_loading,:))
                m=In.M_J(j_loading,var);
                if m>0
                    t_loading = t-In.P_im( In.D.E_k(k),m ) + 1;
                    if t_loading>0
                        A_ineq_PP1A_sub.S_M_loading( (k-1) * par.J * (par.T-1) + (j-1) * (par.T-1)  + t, (m-1) * par.K * par.J *par.T + (k-1) * par.J *par.T + (j_loading-1) * par.T + t_loading)=1;
                    end
                end
            end
        end

        b_ineq_PP1A ((k-1) * par.J * (par.T-1) + (j-1) * (par.T-1)  + t)=1;
    end
end
end
PROBLEM = concatCPLEXinput(PROBLEM, A_ineq_PP1A_sub, b_ineq_PP1A, x_var, 'ineq');
clearvars -except PROBLEM x_var par In big_M opt

%% PP2 Buffer constraint PP2
[A_ineq_PP2_sub, b_ineq_PP2]=createMatrix( par.J * par.K * par.T , x_var);
A_ineq_PP2_sub.I_buffer=    - eye(par.J * par.K * par.T);
PROBLEM = concatCPLEXinput(PROBLEM, A_ineq_PP2_sub, b_ineq_PP2, x_var, 'ineq');
clearvars -except PROBLEM x_var par In big_M opt

%% PP5 Synchronization constraint PP5
[A_eq_PP5_sub, b_eq_PP5]=createMatrix( par.M * par.I * par.J * par.T , x_var);
c=1;
for m=1:par.M 
for i=1:par.I
    for j=1:par.J
        for t=1:par.T
            for var=1:length(In.M_J(j,:))
                if m==In.M_J(j,var) %% m ist zul?ssige Machine f?r j.
                    A_eq_PP5_sub.S_M_prod(c,  (m-1) * par.I *par.T  +  (i-1) * par.T  +  t )  =-1;
                    for k=1:par.K
                       if In.D.E_k(k)==i 
                            A_eq_PP5_sub.S_M_loading(c,  (m-1) * par.K * par.J *par.T  +  (k-1) * par.J *par.T  +  (j-1) * par.T  +  t )  =1;
                       end
                    end
                end
            end
            c=c+1;    
        end
    end
end
end
PROBLEM = concatCPLEXinput(PROBLEM, A_eq_PP5_sub, b_eq_PP5, x_var, 'eq');
clearvars -except PROBLEM x_var par In big_M opt

%% PP3 Parts completion PP3
[A_eq_PP3_sub, b_eq_PP3]=createMatrix( par.K , x_var);
c=1;
for k=1:par.K % Erstellung der einzelnen Matrizen durch abbilden der Gleichungen
if In.D.d_k(k)>par.T
    T_max=par.T;
else 
    T_max=In.D.d_k(k);
end
for t=1:T_max
    for var=1:length(In.M_J(par.J,:))
        j=par.J;
        m=In.M_J(par.J,var);
        if m>0
            t_loading=t-In.P_im(In.D.E_k(k),m);
            if t_loading>0
                A_eq_PP3_sub.S_M_loading( k, (m-1) * par.K * par.J *par.T + (k-1) * par.J *par.T + (j-1) * par.T + t_loading) = par.M_P(m,In.D.E_k(k));
            end
        end
    end
end
b_eq_PP3 (k,1)=In.D.D_k(k);
c=c+1;
end
PROBLEM = concatCPLEXinput(PROBLEM, A_eq_PP3_sub, b_eq_PP3, x_var, 'eq');
clearvars -except PROBLEM x_var par In big_M opt

%% PP4 Parts completion
[A_eq_PP4_sub, b_eq_PP4]=createMatrix( par.K * par.T , x_var);
c=1;
for k=1:par.K % Erstellung der einzelnen Matrizen durch abbilden der Gleichungen
    for t=1:par.T
        A_eq_PP4_sub.I_fin_prod(c, (k-1)* par.T+ t)=-1;
        %number of parts unloaded from final stage
        for var=1:length(In.M_J(par.J,:))
            m=In.M_J(par.J,var);
            if m>0
                t_loading=t-In.P_im(In.D.E_k(k),m);
                if t_loading>0
                    for tau=1:t_loading %Alle bis zum letztm?glichen Zeitpunkt produzierten Produkte
                        A_eq_PP4_sub.S_M_loading( c, (m-1) * par.K * par.J * par.T + (k-1) * par.J * par.T + (par.J-1) * par.T + tau) = par.M_P(m,In.D.E_k(k));
                    end
                end
            end
        end
        c=c+1;
    end
end
PROBLEM = concatCPLEXinput(PROBLEM, A_eq_PP4_sub, b_eq_PP4, x_var, 'eq');
clearvars -except PROBLEM x_var par In big_M opt

%% PP4a job makespan
[A_ineq_PP4a_sub, b_ineq_PP4a]=createMatrix( par.K * par.T + par.K * (par.T-1), x_var);
c=1;
for k=1:par.K % Erstellung der einzelnen Matrizen durch abbilden der Gleichungen
for t=1:par.T
    A_ineq_PP4a_sub.I_fin_prod(c, (k-1)* par.T+ t)=-1;
    for tau=1:t %Alle bis zum letztm?glichen Zeitpunkt produzierten Produkte
        A_ineq_PP4a_sub.S_job_completion(c, (k-1) *par.T + tau)=big_M;
    end
    b_ineq_PP4a (c,1)=-In.D.D_k(k)+big_M;
    c=c+1; 
end
end

for k=1:par.K % Erstellung der einzelnen Matrizen durch abbilden der Gleichungen
for t=2:par.T
    A_ineq_PP4a_sub.I_fin_prod(c, (k-1)* par.T+ t-1)=1;
    A_ineq_PP4a_sub.S_job_completion(c, (k-1) *par.T + t)=1;
    b_ineq_PP4a (c,1)=In.D.D_k(k);
    c=c+1; 
end
end
PROBLEM = concatCPLEXinput(PROBLEM, A_ineq_PP4a_sub, b_ineq_PP4a, x_var, 'ineq');
clearvars -except PROBLEM x_var par In big_M opt

%% PP4b job completion SOS condition
[A_eq_PP4b_sub, b_eq_PP4b]=createMatrix( par.K, x_var);
c=1;
for k=1:par.K % Erstellung der einzelnen Matrizen durch abbilden der Gleichungen
for t=1:par.T
    A_eq_PP4b_sub.S_job_completion(c, (k-1)* par.T+ t)=1; 
end
b_eq_PP4b (c,1)=1;
c=c+1; 
end
PROBLEM = concatCPLEXinput(PROBLEM, A_eq_PP4b_sub, b_eq_PP4b, x_var, 'eq');
clearvars -except PROBLEM x_var par In big_M opt

%% PP4d Machine production constraint after all jobs completed
[A_ineq_PP4d_sub, b_ineq_PP4d]=createMatrix( par.M * 1/2 * par.T, x_var);
c=1;
for m=1:par.M
for t=par.T/2+1:par.T % Erstellung der einzelnen Matrizen durch abbilden der Gleichungen
    tau_max=t-max(In.SUD_im(:,m));
    if tau_max>=1
        for tau=1:tau_max
            A_ineq_PP4d_sub.S_lastjob_completion(c,  tau)=1; 
        end 
    end
    A_ineq_PP4d_sub.S_M_off( c,       (m-1) * par.T + t)        =-1;
    c=c+1;
end
end
PROBLEM = concatCPLEXinput(PROBLEM, A_ineq_PP4d_sub, b_ineq_PP4d, x_var, 'ineq');
clearvars -except PROBLEM x_var par In big_M opt

%% PP4e Last job completion
[A_ineq_PP4e_sub, b_ineq_PP4e]=createMatrix( par.T *2 , x_var);
c=1;
% Erstellung der einzelnen Matrizen durch abbilden der Gleichungen
for t=1:par.T
for tau=1:t
    for k=1:par.K
        A_ineq_PP4e_sub.S_job_completion(c, (k-1)* par.T+ tau)=-1;
    end
    A_ineq_PP4e_sub.S_lastjob_completion(c, tau)=par.K;   
end
b_ineq_PP4e (c,1)=0;
c=c+1;
end
for t=1:par.T
for tau=t:par.T
    for k=1:par.K
        A_ineq_PP4e_sub.S_job_completion(c, (k-1)* par.T+ tau)=-1;
    end
    A_ineq_PP4e_sub.S_lastjob_completion(c, tau)=1;   
end
b_ineq_PP4e (c,1)=0;
c=c+1;
end
PROBLEM = concatCPLEXinput(PROBLEM, A_ineq_PP4e_sub, b_ineq_PP4e, x_var, 'ineq');
clearvars -except PROBLEM x_var par In big_M opt

%% PP4f Last job completion SOS1
[A_eq_PP4f_sub, b_eq_PP4f]=createMatrix( 1, x_var);
c=1;
% Erstellung der einzelnen Matrizen durch abbilden der Gleichungen
for t=1:par.T
A_eq_PP4f_sub.S_lastjob_completion(c, t)=1;
end
b_eq_PP4f (c,1)=1;
PROBLEM = concatCPLEXinput(PROBLEM, A_eq_PP4f_sub, b_eq_PP4f, x_var, 'eq');
clearvars -except PROBLEM x_var par In big_M opt


%% Constraints Energy Production
%% E1 Energy Production E1
[A_eq_E1_sub, b_eq_E1]=createMatrix( par.M * par.T , x_var); %zus?tzliche Bedingung f?r Anfangs und Endedingungen
c=1;
for m=1:par.M % Erstellung der einzelnen Matrizen durch abbilden der Gleichungen
for t=1:par.T
    A_eq_E1_sub.E_cons( c, (m-1) * par.T + t) = -1;
    for i=1:par.I
        A_eq_E1_sub.S_M_idle( c, (m-1) * par.I *par.T + (i-1) * par.T + t)= In.ei_im(i,m) * par.delta_T/60;
        for tau=1:In.P_im(i,m)
            if t-tau + 1 >0
                A_eq_E1_sub.S_M_prod( c, (m-1) * par.I *par.T + (i-1) * par.T + t-tau + 1) = In.ep_imt(i,m,tau) * par.delta_T/60;
            end
        end
        for tau=1:In.SUD_im(i,m)
            if t-tau + 1 >0
                A_eq_E1_sub.S_M_sup( c, (m-1) * par.I *par.T + (i-1) * par.T + t-tau + 1) = In.esud_imt(i,m,tau) * par.delta_T/60;
                A_eq_E1_sub.S_M_sdn( c, (m-1) * par.I *par.T + (i-1) * par.T + t-tau + 1) = In.esud_imt(i,m,tau) * par.delta_T/60;

            end
        end
        for l=1:par.I
            if not(i==l)
                for tau=1:In.A_lim(l,i,m)
                    if t-tau + 1 >0
                        A_eq_E1_sub.S_M_setup( c, (m-1) * par.I * par.I *par.T + (i-1) * par.I *par.T + (l-1) * par.T + t-tau + 1) = In.es_limt(l,i,m,tau) * par.delta_T/60;
                    end
                end
            end
        end
    end
    c=c+1;
end
end
PROBLEM = concatCPLEXinput(PROBLEM, A_eq_E1_sub, b_eq_E1, x_var, 'eq');
clearvars -except PROBLEM x_var par In big_M opt

%% E6 Energy Balance Equations E6
[A_eq_E6_sub, b_eq_E6]=createMatrix(par.T , x_var); %zus?tzliche Bedingung f?r Anfangs und Endedingungen
i=1;
for t=1:par.T % Erstellung der einzelnen Matrizen durch abbilden der Gleichungen
for m=1:par.M
    A_eq_E6_sub.E_cons(i, (m-1) * par.T + t)= 1;
end
A_eq_E6_sub.P_ESScrg( i, t)        = par.delta_T/60;
A_eq_E6_sub.P_ESSdcrg( i, t)       = -par.delta_T/60;
A_eq_E6_sub.E_Pur( i, t)           = -1;
A_eq_E6_sub.E_Grid_sup( i, t)      = 1;
%A_eq_E6_sub.E_PVcurt( i, t)        = 1;
b_eq_E6(i,1)                       = In.E_PV_t(t);
i=i+1;
end
PROBLEM = concatCPLEXinput(PROBLEM, A_eq_E6_sub, b_eq_E6, x_var, 'eq');
clearvars -except PROBLEM x_var par In big_M opt

%% E6a Energy Balance Equations E6
% [A_ineq_E6a_sub, b_ineq_E6a]=createMatrix(par.T , x_var); %zus?tzliche Bedingung f?r Anfangs und Endedingungen
% i=1;
% for t=1:par.T % Erstellung der einzelnen Matrizen durch abbilden der Gleichungen
%     A_ineq_E6a_sub.E_PVcurt( i, t)        = 1;
%     b_ineq_E6a(i,1)                       = In.E_PV_t(t);
%     i=i+1;
% end
% 
% A_ineq_E6a = concatenateMatrix(x_var,A_ineq_E6a_sub);
% clearvars A_ineq_E6a_sub

%% E7 Energy Storage equation E7
[A_eq_E7_sub, b_eq_E7]=createMatrix(par.T +2, x_var); %zus?tzliche Bedingung f?r Anfangs und Endedingungen
c=1;
for t=1:par.T
A_eq_E7_sub.E_ESSstore( c, t + 1)       = -1;
A_eq_E7_sub.E_ESSstore( c, t)           = (1-par.ESS.sigma);
A_eq_E7_sub.P_ESScrg( c, t)             = (par.ESS.eta_c) * par.delta_T/60;
A_eq_E7_sub.P_ESSdcrg( c, t)            = -(1/par.ESS.eta_d) * par.delta_T/60;
c=c+1;
end
%Anfangsbedingung
A_eq_E7_sub.E_ESSstore(c, 1)      = 1; 
b_eq_E7 (c,1)                     = par.ESS.E_0;
c=c+1;
%Endbedingung
A_eq_E7_sub.E_ESSstore(c,par.T + 1)   = 1; 
b_eq_E7 (c,1)                         = par.ESS.E_0;

PROBLEM = concatCPLEXinput(PROBLEM, A_eq_E7_sub, b_eq_E7, x_var, 'eq');
clearvars -except PROBLEM x_var par In big_M opt

%% E8_1 Energy Charge
[A_ineq_E8_sub, b_ineq_E8]=createMatrix(  6*par.T, x_var); %zus?tzliche Bedingung f?r Anfangs und Endedingungen
i=1;
for t=1:par.T
A_ineq_E8_sub.P_ESScrg( i, t)        = 1;
A_ineq_E8_sub.S_ESScrg( i, t)       = -par.ESS.P_c_max;
i=i+1;
end
for t=1:par.T
A_ineq_E8_sub.P_ESScrg( i, t)        = -1;
A_ineq_E8_sub.S_ESScrg( i, t)         = +par.ESS.P_c_min;
i=i+1;
end
for t=1:par.T
A_ineq_E8_sub.P_ESSdcrg( i, t)        = 1;
A_ineq_E8_sub.S_ESSdcrg( i, t)        = -par.ESS.P_d_max;
i=i+1;
end
for t=1:par.T
A_ineq_E8_sub.P_ESSdcrg( i, t)        = -1;
A_ineq_E8_sub.S_ESSdcrg( i, t)        = par.ESS.P_d_min;
i=i+1;
end
for t=1:par.T
A_ineq_E8_sub.P_ESSdcrg( i, t)        = +1;
A_ineq_E8_sub.S_ESSidle( i, t)        = par.ESS.P_d_max;
b_ineq_E8(i,1)                        = par.ESS.P_d_max;
i=i+1;
end
for t=1:par.T
A_ineq_E8_sub.P_ESScrg( i, t)         = 1;
A_ineq_E8_sub.S_ESSidle( i, t)        = par.ESS.P_c_max;
b_ineq_E8(i,1)                        = par.ESS.P_c_max;
i=i+1;
end
PROBLEM = concatCPLEXinput(PROBLEM, A_ineq_E8_sub, b_ineq_E8, x_var, 'ineq');
clearvars -except PROBLEM x_var par In big_M opt

%% E9 BESS State selection 
[A_eq_E9_sub, b_eq_E9]=createMatrix( par.T , x_var); %zus?tzliche Bedingung f?r Anfangs und Endedingungen
i=1;
for t=1:par.T
A_eq_E9_sub.S_ESSdcrg( i, t)        = 1;
A_eq_E9_sub.S_ESScrg( i, t)         = 1;
A_eq_E9_sub.S_ESSidle( i, t)        = 1;
b_eq_E9(i,1)=1;
i=i+1;
end
PROBLEM = concatCPLEXinput(PROBLEM, A_eq_E9_sub, b_eq_E9, x_var, 'eq');
clearvars -except PROBLEM x_var par In big_M opt

%% E10 BESS State selection 
[A_ineq_E10_sub, b_ineq_E10]=createMatrix( 2*par.T , x_var); %zus?tzliche Bedingung f?r Anfangs und Endedingungen
i=1;
for t=1:par.T
A_ineq_E10_sub.S_Grid_sup( i, t)        = -big_M;
A_ineq_E10_sub.E_Grid_sup( i, t)         = 1;
i=i+1;
end
for t=1:par.T
A_ineq_E10_sub.S_Grid_sup( i, t)        = big_M;
A_ineq_E10_sub.E_Pur( i, t)             = 1;
b_ineq_E10(i,1)=big_M;
i=i+1;
end
PROBLEM = concatCPLEXinput(PROBLEM, A_ineq_E10_sub, b_ineq_E10, x_var, 'ineq');
clearvars -except PROBLEM x_var par In big_M opt




end