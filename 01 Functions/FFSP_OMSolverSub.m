function [res, x_var, PROBLEM]=FFSP_OMSolverSub(par, In, opt, x_var, PROBLEM)
big_M=1000000;
%% Flexibility objective function
if opt.FlexCalc==1
    [A_ineq_Flex_sub, b_ineq_Flex]=createMatrix( 2 *(4+par.flex.T_c_fix_inh) , x_var);
    
    
    %upper limit
    c=1;
    tolerance_up=[0.0 0.0 0.0 0.0];
    tolerance_down=[0.01 0.01 0.01 0.01];
    for i=1:1:4
        for t=par.flex.T_c(i):1:par.flex.T_c(i)+par.T_agg_T_mult-1
            A_ineq_Flex_sub.E_Pur( c, t)            = 1;
            A_ineq_Flex_sub.E_Grid_sup( c, t)       = -1;
            A_ineq_Flex_sub.E_deltaFlex( c, t)=-1;
        end
        b_ineq_Flex (c,1)= par.flex.E_T_c_target(i) + abs(par.flex.E_T_c(i)-par.flex.E_T_c_target(i))*tolerance_up(i);
        c=c+1;
        
        %lower limit
        for t=par.flex.T_c(i):1:par.flex.T_c(i)+par.T_agg_T_mult-1
            A_ineq_Flex_sub.E_Pur( c, t) = -1;
            A_ineq_Flex_sub.E_Grid_sup( c, t)       = +1;
            A_ineq_Flex_sub.E_deltaFlex( c, t)=-1;
        end
        b_ineq_Flex (c,1)= -(par.flex.E_T_c_target(i)  - max(abs(par.flex.E_T_c(i)-par.flex.E_T_c_target(i))*tolerance_down(i),1)); %min 1kW zul?ssig
        c=c+1;
    end
    
    if par.flex.T_c_fix_inh>0
        for i=1:par.flex.T_c_fix_inh
            %upper limit
            c=c+1;
            for t=par.flex.T_c_fix_start(i):1:par.flex.T_c_fix_start(i)+par.T_agg_T_mult-1
                A_ineq_Flex_sub.E_Pur( c, t) = 1;
                A_ineq_Flex_sub.E_Grid_sup( c, t)       = -1;
                A_ineq_Flex_sub.E_deltaFlex( c, t)=-1;
            end
            b_ineq_Flex (c,1)= par.flex.E_T_c_fix(i)  + abs(par.flex.E_T_c_fix(i))*0.015;
            
            %lower limit
            c=c+1;
            for t=par.flex.T_c_fix_start(i):1:par.flex.T_c_fix_start(i)+par.T_agg_T_mult-1
                A_ineq_Flex_sub.E_Pur( c, t)  = -1;
                A_ineq_Flex_sub.E_Grid_sup( c, t)       = +1;
                A_ineq_Flex_sub.E_deltaFlex( c, t)      =-1;
            end
            b_ineq_Flex (c,1)= -(par.flex.E_T_c_fix(i)  - abs(par.flex.E_T_c_fix(i))*0.015);
        end
    end
    
    
    PROBLEM = concatCPLEXinput(PROBLEM, A_ineq_Flex_sub, b_ineq_Flex, x_var, 'ineq');
    clearvars -except PROBLEM x_var par In big_M opt
    
    %clearvars A_ineq_Flex_sub A_ineq_Flex
end



%% Optimierung
cplex = Cplex(PROBLEM); %Problem wird in Cplex geschrieben
clearvars -except cplex x_var par In big_M opt

if opt.cplex.DisplayIter==0 %Show Iterations while optimizing
    cplex.DisplayFunc=[];
end
if opt.cplex.TimeCap>0 %Optimization Time Cap
    cplex.Param.timelimit.Cur=opt.cplex.TimeCap;
end
if opt.cplex.SolTolerance>0 %MIP Solution Tolerance setup
    cplex.Param.mip.tolerances.mipgap.Cur=opt.cplex.SolTolerance;
end

cplex.solve;


%% Output Transformation back to readable Format
if strcmp(cplex.Solution.statusstring,'integer infeasible')==1
    res=[];
else
    res = divide_x_v9(cplex.Solution.x ,x_var.dim.vect, x_var.names, x_var.subdim ,  par);
    res.time    =cplex.Solution.time;
    res.mipgap  =cplex.Solution.miprelgap;
    res.status  =cplex.Solution.statusstring;
    res.solution=cplex.Solution;
end

% Aggregate output
res.S_M_esud_MIT=res.S_M_sup_MIT+res.S_M_sdn_MIT; %Startup und shutdown variablen werden zusammengelegt
res.E_cons_TOTAL=0;
for m=1:par.M
    res.E_cons_TOTAL=res.E_cons_TOTAL+res.E_cons_MT(m,:);
end
res.total.E_cons        = sum(res.E_cons_TOTAL);
res.total.E_PV_prod     = sum(In.E_PV_t);
%res.total.E_PV_curt     = sum(res.E_PVcurt_T);
res.total.E_Pur         = sum(res.E_Pur_T);
res.total.E_ESScrg      = sum(res.P_ESScrg_T)*par.delta_T/60;
res.total.E_ESSdrg      = sum(res.P_ESSdcrg_T)*par.delta_T/60;
res.total.E_ESSlost     = res.total.E_ESScrg-res.total.E_ESSdrg;

res.OF_E_Pur=0;
for t=1:par.T
    res.OF_E_Pur = res.OF_E_Pur+    res.E_Pur_T(t)*In.DAM.p_t(t)* par.obj_weights.w_Cost;
end
res.I_CT=0;
for k=1:par.K
    res.I_CT = res.I_CT+res.I_MS_K(k);
end
res.I_CT = res.I_CT + sum(res.E_deltaFlex_T)*big_M;
%res.delta_E=par.flex.delta_E;
end