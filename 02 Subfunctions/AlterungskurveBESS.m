function [perc_lifeloss_cyc] = AlterungskurveBESS(SOC, delta_t)
% SOC in 100 and T in ?C, delta_t ist Schrittweite in sekunden

%% cyc Aging
% Bestimmung der Batteriezyklen ?ber Rainflowz?hlung von Adam Nieslony

SoC_BESS_wendepkt = sig2ext(SOC);
delta_SoC_BESS = rainflow(SoC_BESS_wendepkt);                              % delta_SoC_BESS(1,:) = Amplitude, delta_SoC_BESS(3,:) = Anzahl Halbzyklen
delta_SoC_BESS(1,:) = delta_SoC_BESS(1,:)*2;                               % *100 da in Prozent, *2 da vorher Amplitude und jetzt Cycle Depth

% Zyklische Alterung
[perc_lifeloss_cyc] = sum(delta_SoC_BESS(3,:).*AlterungskurveBESS_cyc(delta_SoC_BESS(1,:), 4)); %zyklische Alterung in %

%% cal Aging
%[perc_lifeloss_cal] = AlterungskurveBESS_cal(SOC, T, delta_t);

%% Addition Gesamtverlust
%perc_lifeloss=perc_lifeloss_cal+perc_lifeloss_cyc;

%LD=1/365/perc_lifeloss;

end