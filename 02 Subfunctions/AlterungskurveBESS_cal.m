function [perc_lifeloss, C, t_EOL] = AlterungskurveBESS_cal(SOC, T, delta_t)
% SOC in 100 and T in �C, t in sekunden
delta_t=delta_t/60/60/24;
T=T+272.15;

% Model II von Max
a=1.009;
b=0.909*10^8;
c=-462.4;
d=0.00385*10^8;
f=-6381;
z=0.6716;
Cmax=46;
C=Cmax*(a-(b*exp(c*SOC.^-1)+d).*exp(f*T.^-1).*delta_t^z);
t_EOL=((a-0.8)*((b*exp(c*SOC.^-1)+d).*exp(f*T.^-1)).^-1).^(1/z);

perc_lifeloss=sum(delta_t*t_EOL.^(-1));

end