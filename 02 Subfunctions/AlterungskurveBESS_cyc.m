function [perc_lifeloss, N_max] = AlterungskurveBESS_cyc(DOD, var)
%ALTERUNGSKURVEBESS returns maximum numbuer of cycles at given depth of

% figure
% grid on
% hold on
% CYC_Depth=[0:1:100];
% plot(CYC_Depth, AlterungskurveBESS_cyc(CYC_Depth,1))
% plot(CYC_Depth, AlterungskurveBESS_cyc(CYC_Depth,2))
% plot(CYC_Depth, AlterungskurveBESS_cyc(CYC_Depth,3))

n_DOD=length(DOD);
if var==1
    
    % Bestimmung maximal m?glicher Zyklen nach Paper von Dirk Magnor und zykl. Alterungsverlauf von MT Jonas Moeljadi
    N_max = 1.77784e6*DOD.^(-1.441);
    perc_lifeloss=N_max.^(-1);
elseif var==2 % Xu_2017_Factoring the Cycle Aging Cost of Batteries Participating in Electricity Markets
    perc_lifeloss=0.000524*(DOD/100).^2.03;
    N_max=perc_lifeloss.^(-1);
    
elseif var==3 % Model taken from Xu et al. 2018 Modeling of Lithium-Ion Batttery Degradation
    k1= 1.40*10^5;
    k2= -0.501;
    k3= -1.23*10^5;
    perc_lifeloss=(k1*(DOD/100).^k2+k3).^(-1);
    N_max=perc_lifeloss.^(-1);    
    
elseif var==4 % Laresgoiti et al. 2015
    perc_lifeloss=0.0004519*(DOD/100).^(1/0.4926);
    N_max=perc_lifeloss.^(-1);  
else
    for n=1:n_DOD

        %taken from Xu_2017_Factoring the Cycle Aging Cost of Batteries Participating in Electricity Markets
        CycleDepth_s=       [0              25              50              62.5        75          87.5        100];
        perc_lifeloss_s=    [0.00000        0.00005        0.000135         0.000215      0.000295     0.000415      0.000565];
        %perc_lifeloss_s=    [0.00000        0.0001        0.00027         0.00043      0.0006     0.0008      0.0013];
        %lineare interpolation der st?tzstellen
        s2=min(CycleDepth_s(CycleDepth_s>=DOD(n)));
        s2_i=find(CycleDepth_s==s2);
        if s2~= 0
            s1_i=   s2_i-1;
            s1=     CycleDepth_s(s1_i);
            r=      (DOD(n)-s1)/(s2-s1);
            perc_lifeloss(n)=perc_lifeloss_s(s1_i)+r*(perc_lifeloss_s(s2_i)-perc_lifeloss_s(s1_i));
            
        else
            perc_lifeloss(n)=perc_lifeloss_s(s2_i);
        end
        N_max(n)=1/perc_lifeloss(n);
    end
    
end

end

