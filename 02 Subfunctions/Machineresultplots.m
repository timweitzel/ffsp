function Machineresultplots(Sim, Sim2, Sim3, Sim4)
if nargin==1
    N2=1;
elseif nargin==2
    N2=2;
elseif nargin==3
    N2=3;
elseif nargin==4
    N2=4;
end
Nplots=4;

figure;
for n2=1:N2
    if n2==1
        In      = Sim.In;
        x_res   = Sim;
        par     = Sim.par;
    elseif n2==2
        In      = Sim2.In;
        x_res   = Sim2;
        par     = Sim2.par;
    elseif n2==3
        In      = Sim3.In;
        x_res   = Sim3;
        par     = Sim3.par;
    elseif n2==4
        In      = Sim4.In;
        x_res   = Sim4;
        par     = Sim4.par;
    end
    % Ab hier sind die "richtigen" Plots
    
    
    %Energy production PV
    hold off;
    subplot(Nplots,N2,(n2-1)+1+N2*0);
    hold all;
    grid on;
    stairs(In.E_PV_t*60/par.delta_T);
    %stairs(x_res.E_PVcurt_T*60/par.delta_T);
    legend('PV');
    ylabel('in kW');
    xlim([1 par.T])
    set(gca,'XTick',      [1:60/par.delta_T:par.T+1])
    set(gca,'XTickLabel', [par.Time.Start_h:1:par.Time.Ende_h])
    
    
    % Plot f?r elektrischen Energiespeicher - Inhalt und Leistungen
    %Energy storage
    hold off;
    subplot(Nplots,N2,(n2-1)+1+N2*1);
    hold all;
    grid on;
    
    
    yyaxis right
    plot(x_res.E_ESSstore_T_1);
    ylim([0 50]);
    ylabel('in kWh');
    
    yyaxis left
    stairs(x_res.P_ESScrg_T-x_res.P_ESSdcrg_T);
    ylabel('in kW');
    ylim([-100 100]);
    
    legend('E^{ESS} in kWh', 'P^{ESS} in kW');
    
    xlim([1 par.T])
    set(gca,'XTick',      [1:60/par.delta_T:par.T+1])
    set(gca,'XTickLabel', [par.Time.Start_h:1:par.Time.Ende_h])
    
    
    
    
    %legend('E^{bat}', 'P^{(ch)}', 'P^{(dch)}', 'z^{charge}', 'z^{discharge}', 'P_{curtail} (PV)');
    legend('P^{ESS}_{elec}', 'E^{ESS}_{elec}');
    
    
    % Energy Consumption Production Total
    hold off;
    subplot(Nplots,N2,(n2-1)+1+N2*2);
    hold all;
    grid on
    
    maxY=0;
    stairs((x_res.E_Pur_T-x_res.E_Grid_sup_T)/par.delta_T*60, '-');
    [output_sum, output_average_P, output_average_input]=averagingOutput (x_res.E_Pur_T-x_res.E_Grid_sup_T, 30/par.delta_T, par.delta_T/60);
    stairs(output_average_input/par.delta_T*60, '-');
    maxY=max(maxY,max(x_res.E_Pur_T/par.delta_T*60));
    %ylim([0 Q_max_S]);
    ylabel('in kW')
    legend('5min average', '30min average');
    
    xlim([1 par.T])
    set(gca,'XTick',      [1:60/par.delta_T:par.T+1])
    set(gca,'XTickLabel', [par.Time.Start_h:1:par.Time.Ende_h])
    ylim([-50 250]);
    clear max
    
    % Energy Consumption Production
    hold off;
    subplot(Nplots,N2,(n2-1)+1+N2*3);
    hold all;
    grid on
    name={};
    maxY=0;
    Y=[];
    for m=1:1:par.M
        Y=[Y;x_res.E_cons_MT(m,:)/par.delta_T*60];
        %stairs(x_res.E_cons_MT(m,:)/par.delta_T*60, '-');
        
        name=[name {strcat('Machine ', num2str(m))}];
        maxY=max(maxY,max(x_res.E_cons_MT(m,:)/par.delta_T*60));
    end
    area(Y');
    color_machines=get(gca,'colororder');    
    colormap(color_machines);
    %ylim([0 Q_max_S]);
    legend(name);
    ylabel('in kW')
    xlabel('time');
    xlim([1 par.T])
    set(gca,'XTick',      [1:60/par.delta_T:par.T+1])
    set(gca,'XTickLabel', [par.Time.Start_h:1:par.Time.Ende_h])
    ylim([0 max(sum(Y))*1.2]);
    clear max
    
   
        
end       
        
end