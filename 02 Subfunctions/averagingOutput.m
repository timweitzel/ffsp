function [output_sum, output_average_P, output_average_input]=averagingOutput (input, delta_T_mult, t_in_hours)
%% input should be kwh per time unit. delta_T_mult is the share of the time fraction that it should be aggregated to. Example: 10min energy consumption in kwh should be aggregated to 1h intervalls. delta_T_mult should be 6 then.

num_in  =numel(input);
num_out =num_in/delta_T_mult;
for o=1:num_out
    output_sum(o)=0;
    for i=(o-1)*delta_T_mult+1:(o-1)*delta_T_mult+delta_T_mult;
        output_sum(o)=output_sum(o)+input(i)*t_in_hours;
    end
    output_average_P(o)=output_sum(o)/(t_in_hours*delta_T_mult);
    for i=(o-1)*delta_T_mult+1:(o-1)*delta_T_mult+delta_T_mult;
        output_average_input(i)=output_average_P(o);
    end
end

end