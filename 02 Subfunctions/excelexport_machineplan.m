function res=excelexport_machineplan (Sim)

In=Sim.In;
x_res=Sim; 
par=Sim.par;
%% Machine status plan
N=par.I * (par.I-1);
if par.I==1
    N=1;
end

table_offset=6;

par.MModes=5;
Mode=par.MModes;
M1=Mode-1+N-1-1;

res_machine_status_stim = zeros(par.M * par.I * M1 + par.M,par.T + table_offset);
for m=1:par.M
    for t=1:par.T

        res_machine_status_stim((m-1) * par.I * M1 + par.I * M1 + 1 + m-1 ,1)=m;
        res_machine_status_stim((m-1) * par.I * M1 + par.I * M1 + 1 + m-1 ,2)=0;
        res_machine_status_stim((m-1) * par.I * M1 + par.I * M1 + 1 + m-1 ,4)=5;
        res_machine_status_stim((m-1) * par.I * M1 + par.I * M1 + 1 + m-1 ,t + table_offset)=  int16(x_res.S_M_off_MT(m,t));
        sum= res_machine_status_stim((m-1) * par.I * M1 + par.I * M1 + 1 + m-1 ,t + table_offset);       
        for i=1:par.I 
           
           res_machine_status_stim((m-1) * par.I * M1 + (i-1) * M1 + 1 + (m-1)     , t + table_offset)=  int16(x_res.S_M_prod_MIT(m,i,t));
           res_machine_status_stim((m-1) * par.I * M1 + (i-1) * M1 + 1 + (m-1)     , 1)=m;
           res_machine_status_stim((m-1) * par.I * M1 + (i-1) * M1 + 1 + (m-1)     , 2)=i;
           res_machine_status_stim((m-1) * par.I * M1 + (i-1) * M1 + 1 + (m-1)     , 4)=1;
           
           res_machine_status_stim((m-1) * par.I * M1 + (i-1) * M1 + 2 + (m-1)     , t + table_offset)=  int16(x_res.S_M_idle_MIT(m,i,t));
           res_machine_status_stim((m-1) * par.I * M1 + (i-1) * M1 + 2 + (m-1)     , 1)=m;
           res_machine_status_stim((m-1) * par.I * M1 + (i-1) * M1 + 2 + (m-1)     , 2)=i;
           res_machine_status_stim((m-1) * par.I * M1 + (i-1) * M1 + 2 + (m-1)     , 4)=2;
           
           res_machine_status_stim((m-1) * par.I * M1 + (i-1) * M1 + 3 + (m-1)     ,1)=m;
           res_machine_status_stim((m-1) * par.I * M1 + (i-1) * M1 + 3 + (m-1)     ,2)=i;
           res_machine_status_stim((m-1) * par.I * M1 + (i-1) * M1 + 3 + (m-1)     ,4)=4;
           res_machine_status_stim((m-1) * par.I * M1 + (i-1) * M1 + 3 + (m-1)     ,t + table_offset)=  int16(x_res.S_M_esud_MIT(m,i,t));
                      
           n=4;
           for l=1:par.I
               if not(l==i)
                res_machine_status_stim((m-1) * par.I * M1 + (i-1) * M1 + n + (m-1)     ,t + table_offset)= int16(x_res.S_M_setup_MIIT(m,i,l,t));    
                res_machine_status_stim((m-1) * par.I * M1 + (i-1) * M1 + n + (m-1)     ,1)=m;
                res_machine_status_stim((m-1) * par.I * M1 + (i-1) * M1 + n + (m-1)     ,2)=i;
                res_machine_status_stim((m-1) * par.I * M1 + (i-1) * M1 + n + (m-1)     ,3)=l;
                res_machine_status_stim((m-1) * par.I * M1 + (i-1) * M1 + n + (m-1)     ,4)=3;
                
                sum = sum  +  res_machine_status_stim((m-1) * par.I * M1 + (i-1) * M1 + n + (m-1)     ,t + table_offset);
                n=n + 1;
               end
           end
          sum = sum + res_machine_status_stim((m-1) * par.I * M1 + (i-1) * M1 + 1 + (m-1)     ,t + table_offset);
          sum = sum + res_machine_status_stim((m-1) * par.I * M1 + (i-1) * M1 + 2 + (m-1)     ,t + table_offset);
          sum = sum + res_machine_status_stim((m-1) * par.I * M1 + (i-1) * M1 + 3 + (m-1)     ,t + table_offset);
        end  
        if sum==0 && t>1
           for i=1:par.I
               if   res_machine_status_stim((m-1) * par.I * M1 + (i-1) * M1 + 1 + (m-1)     ,t-1 + table_offset)>0
                    res_machine_status_stim((m-1) * par.I * M1 + (i-1) * M1 + 1 + (m-1)     ,t + table_offset)=1;
               end
               if   res_machine_status_stim((m-1) * par.I * M1 + (i-1) * M1 + 2 + (m-1)     ,t-1 + table_offset)>0
                    res_machine_status_stim((m-1) * par.I * M1 + (i-1) * M1 + 2 + (m-1)     ,t + table_offset)=1;
               end
               if   res_machine_status_stim((m-1) * par.I * M1 + (i-1) * M1 + 3 + (m-1)     ,t-1 + table_offset)>0
                    res_machine_status_stim((m-1) * par.I * M1 + (i-1) * M1 + 3 + (m-1)     ,t + table_offset)=1;
               end

               n=4;
               for l=1:par.I
                   if not(l==i)
                        if res_machine_status_stim((m-1) * par.I * M1 + (i-1) * M1 + n + (m-1)     ,t-1 + table_offset)>0 && res_machine_status_stim((m-1) * par.I * M1 + (i-1) * M1 + n + (m-1)     ,table_offset + t-In.A_lim(l,i,m))==0
                           res_machine_status_stim((m-1) * par.I * M1 + (i-1) * M1 + n + (m-1)     ,t + table_offset)=1;
                        end
                    n=n + 1;
                   end
               end
           end
        end
    end
end


Names={'Machine' 'Product1' 'Product2' 'Modes' 'Stage' 'Order' };

for t=1:par.T
    Names=[Names, {strcat('T_', num2str(t))}];   
end

res.machine_status_table=array2table(res_machine_status_stim);
res.machine_status_table.Properties.VariableNames=Names;

%% Create Machine Plan plot

% Create Matrix per machine and mode (production, idle, setup, startup, off)

res.plot.Machineplan_complete=zeros(par.M * par.MModes,par.T);
for n=1: length(res_machine_status_stim(:,1))
       m        =   res_machine_status_stim(n,1);
       Mode     =   res_machine_status_stim(n,4);
       res.plot.Machineplan_complete((m-1) * par.MModes + Mode,:)= res.plot.Machineplan_complete((m-1) * par.MModes + Mode,:)+ res_machine_status_stim(n,table_offset + 1:end);
end


%% Create Machine Plan plot 2

res.plot.Machineplan_prod=zeros(par.M ,par.T + table_offset);
tbl=res.machine_status_table;
for i=1:par.I
    res.plot.Machineplan_prod=res.plot.Machineplan_prod + i * table2array(tbl( (tbl.Modes==1) & (tbl.Product1==i),:));
end
    




%% Power consumption per Machine

res_E_consProd_m = zeros(par.M,par.T + table_offset);
for m=1:par.M
    res_E_consProd_m(m,table_offset + 1:end)=   x_res.E_cons_MT(m,:);
     res_E_consProd_m(m,1)=m;
end
res_E_consProd_table=array2table(res_E_consProd_m);
res_E_consProd_table.Properties.VariableNames=Names;


%% Buffer per order
res_buffer_status_st = zeros(par.K * par.J,par.T + table_offset);
for k=1:par.K
    for j=1:par.J
        res_buffer_status_st((k-1) * par.J + j,table_offset + 1:end)=x_res.I_buffer_KJT(k,j,:);
        res_buffer_status_st((k-1) * par.J + j,6)=k;
        res_buffer_status_st((k-1) * par.J + j,5)=j;
        res_buffer_status_st((k-1) * par.J + j,2)=In.D.E_k(k); 
    end
end
res.buffer_status_table=array2table(res_buffer_status_st);
res.buffer_status_table.Properties.VariableNames=Names;


%% Loading table
res_loading_status_st = zeros(par.K * par.J * par.M,par.T + table_offset);
for m=1:par.M
    for k=1:par.K
        for j=1:par.J
            res_loading_status_st((k-1) * par.J * par.M + (j-1) * par.M + m,table_offset + 1:end)=  x_res.S_M_loading_MKJT(m,k,j,:);
            res_loading_status_st((k-1) * par.J * par.M + (j-1) * par.M + m,1)=m;
            res_loading_status_st((k-1) * par.J * par.M + (j-1) * par.M + m,6)=k;
            res_loading_status_st((k-1) * par.J * par.M + (j-1) * par.M + m,5)=j;
            res_loading_status_st((k-1) * par.J * par.M + (j-1) * par.M + m,2)=In.D.E_k(k);
        end
    end
end
res.loading_status_table=array2table(res_loading_status_st);
res.loading_status_table.Properties.VariableNames=Names;




%% Ablage Tables in Excel Document

folderold=pwd;
%folder='/Users/Timm/Dropbox/Promotion/12 Production System Flexibility Provision/02 Simulation Results/Excel';
folder='C:\Users\weitzel\Dropbox\Promotion\12 Production System Flexibility Provision\02 Simulation Results\Excel';
cd(folder);

filename = strcat('outputdata_',datestr(now,'yy-mm-dd_HH-MM-SS'),'.xlsx');

ident=1;
Table=res.machine_status_table;
writetable(Table,filename,'Sheet',1,'Range',strcat('A', num2str(ident) ));

ident= ident  +  length(res.machine_status_table.Machine)  +  3;
Table=res.loading_status_table;
writetable(Table,filename,'Sheet',1,'Range',strcat('A', num2str(ident) ));

ident= ident  +  length(res.loading_status_table.Machine)  +  3;
Table=res.buffer_status_table;
writetable(Table,filename,'Sheet',1,'Range',strcat('A', num2str(ident) ));

ident= ident  +  length(res.buffer_status_table.Machine)  +  3;
Table=res_E_consProd_table;
writetable(Table,filename,'Sheet',1,'Range',strcat('A', num2str(ident) ));

plotMachineplan(par.M,par.T,par.delta_T ,5,res.plot.Machineplan_complete, par.Time.Start_h, In, par);

cd(folderold);