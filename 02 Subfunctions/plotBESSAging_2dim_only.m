function plotBESSAging_2dim_only(res, s_print, printname, path)

[N1,N2,H]=size(res.deltaE);

% plot objective curve
figure

for i=1:1:H
    subplot(1,ceil(H/2),i)
    hold on
    grid on
    name={};
    for n1=1:N1
        value=res.deltaE(n1,1,i)/30*60;
        plot(res.deltaE2(n1,:,i)/30*60,res.BESS_lifeloss(n1,:,i),'-o')
        xlim([min(res.deltaE2(n1,:,i)/30*60)-10 max(res.deltaE2(n1,:,i)/30*60)+10]);
        %ylim([950 1100]);
        name=[name {strcat('\Delta P_{DRP,n=1}=',num2str(value),'kW')}];
        %ylim([min(res.CT(n1,:,i))*0.95 min(res.CT(n1,:,i))*1.2]);
    end
    xl=xlabel('\Delta P_{DRP,n=2} [in kW]');
    yl=ylabel(strcat('BESS Lifeloss [in %]'));
    %set(gca,'YTick',      [950:25:1100])
    %title(strcat(num2str(N_h(i)),':00-',num2str(N_h(i)+1),':00'));
    legend(name, 'Location','northeast')

    xl.FontSize=12;
    yl.FontSize=12;

end

if s_print ==1
    set(gcf,'Units','centimeters','PaperPosition',[0 0 8 5],'PaperPositionMode','manual','PaperSize',[10 5])
    print(gcf,'-r300','-dtiff', '-opengl',strcat(path,printname))
end
end

