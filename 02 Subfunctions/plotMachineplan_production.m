function plotMachineplan_production(Sim, s_print, printname, path)

In=Sim.In;
par=Sim.par;
T_Start_h=par.Time.Start_h;
Modes=1;
%clear all;

for m=1:par.M
    for j=1:par.J
        for n=1:numel(In.M_J(j,:))
            if m==In.M_J(j,n)
                J_M(m)=j;
            end
        end
    end
end

% M=2;
% T=5;
% Modes=3;
% I=1;
% Machineplan = [1,1,1,0,0,;0,0,0,1,1,;0,0,0,0,0;0,0,0,1,1,;1,1,1,0,0;0,0,0,0,0];

color=[ 236 146 13; 12 96 244;  0 204 0; 51 153 255; 84 84 84]/255;
color_stages=[ 220 220 220; 192 192 192;  128 128 128; 105 105 105;  0 0 0]/255;
color_machines=get(gca,'colororder');    

N=par.M*Modes;

%%Code
plotSol = 0;
smallScale = true;
% if nargin > 4
%     plotSol = 1;
%     xsol = varargin{1};
%     if nargin > 5
%         smallScale = varargin{2};
%     end
% end

nPtotal = N*par.T;%length(PurchaseYears);


%% Create a figure
figure('Position',[20 100 700+25*par.T 100+25*N],'Color','w','Units','Pixels')

%% Create an axis
ax = gca;
% Set the axes to fill the entire figure
set(ax,'Position',[0 0 1 1]);
% remove the axis ticks
axis off

hold on

% Define x- and y-spacing for the timeslots
x=zeros(par.T+2,1);
x(1)=0.09;
x2=0.15;
x(2:par.T+2) = linspace(x2,0.98,par.T+1);
xwidth = x(3)-x(2);
xl = 0.02;

yt = 0.98; % Start oberes Ende
ywidth_sep=0.02; % Breite des Separator
ylength_total=0.90; % Bereiche der Grafik
ywidth= (ylength_total-ywidth_sep*(par.M-1))/(par.M*Modes);

y(1)=0.92;
y_i=2;
while y(y_i-1)>0.02
    for m=1:par.M
        for j=1:Modes
            y(y_i)=y(y_i-1)-ywidth;
            y_i=y_i+1;
            if j==Modes && m<par.M %Hier wird der Trenner hinzugef?gt
                y(y_i)=y(y_i-1)-ywidth_sep;
                y_i=y_i+1;
            end
        end
    end
end


% Define x-spacing for the vertical lines
x_vl=zeros(par.T/6+2,1);
x_vl(1)=x(1);
x_vl(2:par.T/6+2) = linspace(x2,0.98,par.T/6+1);
xwidth_vl = x_vl(3)-x_vl(2);
xl_vl = 0.02;
yt_vl = 0.98;
y_vl = linspace(0.92,0.02,N+1);
%y = linspace(0.84,0.02,N+1);
%y2= linspace(0.84/I/Modes,0.02,N+1);
ywidth0_vl = y_vl(1)-y_vl(2);
ywidth_vl = y_vl(2)-y_vl(3);

% Plot the vertical timeslot lines
xyear_vl = NaN(3*(par.T/6+1)+2,1);
xyear_vl(1:3:end) = x_vl;
xyear_vl(2:3:end) = x_vl;
yyear_vl = NaN(3*(par.T/6+1)+2,1);
yyear_vl(1:3:end) = yt_vl;
yyear_vl(2:3:end) = y_vl(end);
plot(xyear_vl,yyear_vl,'k--')
if smallScale
    plot([xl_vl xl_vl],[y_vl(1) y_vl(end)],'k')
end


% Plot the horizontal machine lines
xsec = NaN(3*(numel(y)),1);
xsec(1:3:end) = xl;
xsec(2:3:end) = x(end);
ysec = NaN(3*(numel(y)),1);
ysec(1:3:end) = y;
ysec(2:3:end) = y;
plot(xsec,ysec,'k')
plot([x(1) x(end)], [yt yt],'k')

% Add machine textbox
ar = annotation('rectangle','Position',[xl y(1) x(1)-xl yt-y(1)],...
        'FaceColor', [0.97 0.97 0.97]);
    annotation('textbox','Position',[xl y(1) x(1)-xl yt-y(1)],...
        'String', ['Machine'],...
        'VerticalAlignment','Middle', 'HorizontalAlignment','Center',...
        'EdgeColor','none');

% Add stages textbox
ar = annotation('rectangle','Position',[x(1) y(1) x(2)-x(1) yt-y(1)],...
        'FaceColor', [0.97 0.97 0.97]);
    annotation('textbox','Position',[x(1) y(1) x(2)-x(1) yt-y(1)],...
        'String', [ 'Stage'],...
        'VerticalAlignment','Middle', 'HorizontalAlignment','Center',...
        'EdgeColor','none');

% Add textbox to label each timeslot
for j=1:par.T*par.delta_T/60
    ar = annotation('rectangle','Position',[x((j-1)*60/par.delta_T+2) y(1) xwidth*60/par.delta_T yt-y(1)],...
        'FaceColor', [0.97 0.97 0.97]);
    annotation('textbox','Position',[x((j-1)*60/par.delta_T+2) y(1) xwidth*60/par.delta_T yt-y(1)],...
        'String', [ '' num2str(j+T_Start_h-1)],...
        'VerticalAlignment','Middle', 'HorizontalAlignment','Center',...
        'EdgeColor','none');
end

%% Add textbox to label each machine
for m=1:par.M
    ar = annotation('rectangle','Position',[xl y((m-1)*Modes+(m-1)+2) x(1)-xl ywidth*Modes],...
        'FaceColor', color_machines(m,:));
    annotation('textbox','Position',[xl y((m-1)*Modes+(m-1)+2) x(1)-xl ywidth*Modes],...
        'String',['#' num2str(m) char(10) ],...
        'VerticalAlignment','Middle', 'HorizontalAlignment','Center',...
        'EdgeColor','none');
end

%% Add stage label
for m=1:par.M
    ar = annotation('rectangle','Position',[x(1) y((m-1)*Modes+(m-1)+2) x(2)-x(1) ywidth*Modes],...
        'FaceColor', color_stages(J_M(m),:));
    annotation('textbox','Position',[x(1) y((m-1)*Modes+(m-1)+2) x(2)-x(1) ywidth*Modes],...
        'String',['#' num2str(J_M(m)) char(10) ],...
        'VerticalAlignment','Middle', 'HorizontalAlignment','Center',...
        'EdgeColor','none');
end




%% Add textbox to label each stage
% for m=1:par.M
%     for j=1:Modes
%         switch j
%             case 1
%                 stagename='';
%             case 2
%                 stagename='Production';
%             case 3
%                 stagename='';
%             case 4
%                 stagename='';
%             case 5
%                 stagename='';
%         end
%         
%         annotation('textbox','Position',[x(1) y((m-1)*Modes+j+1) x(1)-xl ywidth],...
%             'String',['' stagename ],...
%             'VerticalAlignment','Middle', 'HorizontalAlignment','left',...
%             'EdgeColor','none');
%     end
% end

% Plot the Machineplan

% Start with the first machine on the first line
%alternator=1;

for m=1:par.M
    for j=1:Modes
        for t=1:par.T
            for i=1:par.I
                if Sim.S_M_prod_MIT(m,i,t)>0.5
                    ar = annotation('rectangle','Position',[x(t+1) y((m-1)*Modes+(m-1)+2) x(t+1+In.P_im(i,m))-x(t+1) ywidth],...
                        'FaceColor', color(i,:));
                end
            end
        end
    end
end


hold off

if s_print ==1
    set(gcf,'Units','centimeters','PaperPosition',[0 0 10 4],'PaperPositionMode','manual','PaperSize',[10 4])
    print(gcf,'-r300','-dtiff', '-opengl',strcat(path,printname))
end