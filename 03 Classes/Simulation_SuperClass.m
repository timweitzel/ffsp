classdef Simulation_SuperClass
    %SIMULATION_SUPERCLASS Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
            Base,... % Type Simulation for base case
            Simulations, ... % Type simulation for every simulation
            N_Base, ... % number of base cases
            N_Sim, ... % number of simulations for flex
            par, ... % parameter for base
            opt, ... % options for base
            In, ... % input for base
            folder_store,... %foler string for save
            folder_sim, ... %base folder with simulation files
            folder_graphs, ... %base folder with simulation files
            par_flex, ... %par file for flex simulation
            opt_flex, ...%opt file for flex simulation
            In_flex, ... %input file for flex simulation
            PROBLEM, ... %Problem file for flex simulation
            x_var,... % variable definition to pass through
            Flex_Sum_res,... % summary file for flex simulation
            Flex_Sum_res_real,...
            Savename
    end
    
    methods
        function obj = update_folder(obj,sel)
            folder=Folderlocations(sel);
            obj.folder_store =  folder.store;
            obj.folder_sim   =  folder.sim;
            obj.folder_graphs=  folder.graphs;
        end
        
        function obj = Simulation_SuperClass(par,In,opt, folder_store, folder_sim)
            obj.N_Sim      =0;
            obj.N_Base     =0;
            obj.par        =par;
            obj.opt        =opt;
            obj.In         =In;
            obj.folder_store      =folder_store;
            obj.folder_sim  =folder_sim;
            obj.folder_graphs = folder_store;
            obj.Savename    =strcat(num2str(yyyymmdd(datetime('now'))), '_ESS', num2str(obj.par.ESS.E_max));
        end

        function obj = createBaseSim(obj)
            %Adds New simulation as base case with ID 0
            obj.N_Base                  = obj.N_Base+1;  
            Base  = Simulation(0, obj.par, obj.In, obj.opt, 'Base Case');
            try
                Base=Base.solve;
            catch
            end
            obj.Base = Base;
            obj.save
        end
        
       function obj = solveSelectedSim(obj,vect)
            %Solves simulation for vector of IDs
            [obj.PROBLEM, obj.x_var]=FFSP_OM(obj.par_flex, obj.In_flex, obj.opt_flex);
            for i=1:length(vect)
                try
                    disp(vect(i));
                    obj.Simulations(vect(i))=obj.Simulations(vect(i)).solve(obj.PROBLEM, obj.x_var);
                catch
                    disp('Simulation not completed');
                end
            end
            obj.save
            obj.PROBLEM=[];
        end
        
        
        %% Simulation of Flexibility
        function obj = Add_flex(obj, par_flex, In_flex, opt_flex)
            obj.par_flex    =par_flex;
            obj.opt_flex    =opt_flex;
            obj.In_flex     =In_flex;
            
            obj.par_flex.flex.T_c_inh_vect_num   =numel(obj.par_flex.flex.T_c_inh_vect); %number of instances with flexibility variation
        end
        
        function obj = FlexSim(obj)
            tges=datetime('now');
            parDt   =   obj.par_flex.delta_T; 
         
            for fl_int= 1:obj.par_flex.flex.T_c_inh_vect_num
                %Definition receding horizon parameter
                obj.par_flex.T_RH_inh       =obj.par_flex.flex.T_c_inh_vect(fl_int); %1h vor flexibilisierungsstunde
                obj.par_flex.T_RH_vorFlex   =ceil(obj.par_flex.T_RH_vorFlex_inmin/parDt);
                obj.par_flex.T_RH           =(obj.par_flex.T_RH_inh-(obj.par_flex.Time.Start_h))/(parDt/60)-obj.par_flex.T_S-obj.par_flex.T_RH_vorFlex;
                
                %Auswahl neuer Basis
                obj.In_flex.RecedingHorizon=     obj.Base.x_res; %Input for receding horizon
                
                %% Problem is generated for faster variant calculation
                [obj.PROBLEM, obj.x_var]=FFSP_OM(obj.par_flex, obj.In_flex, obj.opt_flex);
                
                %Bestimmung zu reduzierenden Stunde

                output_sum                      = averagingOutput ((obj.Base.x_res.E_Pur_T-obj.Base.x_res.E_Grid_sup_T)/parDt*60, 30/parDt, parDt/60); %Output wird hier auf stundenniveaus aggregiert um zielgr??e zu definieren
                obj.par_flex.flex.T_c_inh       = obj.par_flex.flex.T_c_inh_vect(fl_int);
                obj.par_flex.flex.T_c           = zeros(4,1);
                obj.par_flex.flex.T_c(2)        = (obj.par_flex.flex.T_c_inh-obj.par_flex.Time.Start_h)/(parDt/60)+1; % target value
                obj.par_flex.flex.T_c(1)        = obj.par_flex.flex.T_c(2)-obj.par_flex.T_agg_T_mult; % preperiod
                obj.par_flex.flex.T_c(3)        = obj.par_flex.flex.T_c(2)+obj.par_flex.T_agg_T_mult; % following period
                obj.par_flex.flex.T_c(4)        = obj.par_flex.flex.T_c(2)-2*obj.par_flex.T_agg_T_mult; % preperiod-1
                obj.par_flex.flex.E_T_c(1)      = output_sum(obj.par_flex.flex.T_c_inh*60/obj.par_flex.T_agg_T-obj.par_flex.Time.Start_h*60/obj.par_flex.T_agg_T+0); % preperiod
                obj.par_flex.flex.E_T_c(2)      = output_sum(obj.par_flex.flex.T_c_inh*60/obj.par_flex.T_agg_T-obj.par_flex.Time.Start_h*60/obj.par_flex.T_agg_T+1); % target value
                obj.par_flex.flex.E_T_c(3)      = output_sum(obj.par_flex.flex.T_c_inh*60/obj.par_flex.T_agg_T-obj.par_flex.Time.Start_h*60/obj.par_flex.T_agg_T+2); % following period
                obj.par_flex.flex.E_T_c(4)      = output_sum(obj.par_flex.flex.T_c_inh*60/obj.par_flex.T_agg_T-obj.par_flex.Time.Start_h*60/obj.par_flex.T_agg_T-1); % preperiod-1
                obj.par_flex.flex.T_c_fix_inh   = 0;
                for i=1:obj.par_flex.flex.T_c_fix_inh
                    obj.par_flex.flex.E_T_c_fix(i)       =output_sum(obj.par_flex.flex.T_c_inh-obj.par_flex.Time.Start_h+1+i);
                    obj.par_flex.flex.T_c_fix_start(i)   =(obj.par_flex.flex.T_c_inh+i-obj.par_flex.Time.Start_h)/(parDt/60)+1;
                end
                
                obj.par_flex.flex.eps.num1  = numel(obj.par_flex.flex.eps.delta1);
                obj.par_flex.flex.eps.num2  = numel(obj.par_flex.flex.eps.delta2);
                obj.par_flex.flex.eps.num   = obj.par_flex.flex.eps.num1* obj.par_flex.flex.eps.num2;
                
                %% Flexibility simulations are generated and calculated
                et=1;
                obj.N_Sim =0;
                for e1=1:obj.par_flex.flex.eps.num1
                    for e2=1:obj.par_flex.flex.eps.num2
                        obj.par_flex.flex.delta_E                   =    obj.par_flex.flex.eps.delta1(e1);
                        obj.par_flex.flex.delta_E2                  =    obj.par_flex.flex.eps.delta2(e2);
                        obj.par_flex.flex.E_T_c_target(1)           =    max(-100,obj.par_flex.flex.E_T_c(1)); %falls E_T_C bereits null werden
                        obj.par_flex.flex.E_T_c_target(2)           =    max(-100,obj.par_flex.flex.E_T_c(2)+obj.par_flex.flex.delta_E); %falls E_T_C bereits null werden
                        obj.par_flex.flex.E_T_c_target(3)           =    max(-100,obj.par_flex.flex.E_T_c(3)+obj.par_flex.flex.eps.delta2(e2)); %falls E_T_C bereits null werden
                        obj.par_flex.flex.E_T_c_target(4)           =    max(-100,obj.par_flex.flex.E_T_c(4)); %falls E_T_C bereits null werden
                        
                        if ((1+obj.par_flex.flex.eps.delta1(e1))==1) && ((1+obj.par_flex.flex.eps.delta2(e2))==1)
                            obj.N_Sim                  = obj.N_Sim+1;
                            Sim_local(obj.N_Sim)       = obj.Base;
                            Sim_local(obj.N_Sim).ID    = obj.N_Sim;
                            %res=obj.Base.x_res;
                        else
                            obj.N_Sim                  = obj.N_Sim+1;
                            Sim_local(obj.N_Sim)=     Simulation(obj.N_Sim, obj.par_flex, obj.In_flex, obj.opt_flex, strcat('Flex Case ', num2str(obj.N_Sim)));
                            Sim_local(obj.N_Sim)=     Sim_local(obj.N_Sim).solve(obj.PROBLEM, obj.x_var);
                            %res=FFSP_OMSolverSub(ob.par_flex, obj.In_flex, obj.opt_flex, x_var, PROBLEM);
                            res=Sim_local(obj.N_Sim).x_res;
                        end
                        
                        
                        S=sprintf(strcat('FlexHour: '    , num2str(fl_int),'/',num2str(obj.par_flex.flex.T_c_inh_vect_num),...
                            ' -- DeltaE: '   , num2str(et),'/',num2str(obj.par_flex.flex.eps.num),...
                            ' -- t_iter: '   , num2str(Sim_local(obj.N_Sim).time),'in s', ...
                            ' -- gap: '      , num2str( ceil(Sim_local(obj.N_Sim).mipgap*1000)/10 ), ' %%',...
                            ' -- status: '   , Sim_local(obj.N_Sim).status),...
                            ' -- t_ges: '    , datestr(datetime('now')-tges,'dd:HH:MM:SS'));
                        disp(S);
                        
                        obj.Flex_Sum_res.deltaE(e1,e2)  =   obj.par_flex.flex.eps.delta1(e1);
                        obj.Flex_Sum_res.deltaE2(e1,e2) =   obj.par_flex.flex.eps.delta2(e2);
                        obj.Flex_Sum_res.CT(e1,e2)      =   Sim_local(obj.N_Sim).x_res.I_CT;
                        obj.Flex_Sum_res.ID(e1,e2)      =   Sim_local(obj.N_Sim).ID;
                        
                        Sim_local(obj.N_Sim).delta_E1 =obj.Flex_Sum_res.deltaE(e1,e2);
                        Sim_local(obj.N_Sim).delta_E2 =obj.Flex_Sum_res.deltaE2(e1,e2);
                        Sim_local(obj.N_Sim).fl_int   =fl_int;
                        et=et+1;
                    end
                    
                end
                obj.Simulations=Sim_local;
            end
            obj.PROBLEM=[];
            try
                obj.save
            catch
                disp('Save not successfull');
            end
        end
        
        
        function obj =calc_Flex_sum_res(obj)
            for e1=1:obj.par_flex.flex.eps.num1
                    for e2=1:obj.par_flex.flex.eps.num2
                        ID=obj.Flex_Sum_res.ID(e1,e2);
                        Sim=obj.Simulations(ID);
                        obj.Flex_Sum_res.deltaE(e1,e2)  =   Sim.par.flex.delta_E;
                        obj.Flex_Sum_res.deltaE2(e1,e2) =   Sim.par.flex.delta_E2;
                        obj.Flex_Sum_res.CT(e1,e2)      =   Sim.x_res.I_CT;
                        obj.Flex_Sum_res.ID(e1,e2)      =   Sim.ID;
                        obj.Flex_Sum_res.mipgap_100(e1,e2)  =   Sim.mipgap*100;
                        
                    end
            end
            
        end
        
        
        function obj =calc_Flex_sum_res_real(obj)
            for e1=1:obj.par_flex.flex.eps.num1
                    for e2=1:obj.par_flex.flex.eps.num2
                        ID=obj.Flex_Sum_res.ID(e1,e2);
                        Sim=obj.Simulations(ID);
                        [output_sum, output_average_P, output_average_input]=averagingOutput (Sim.x_res.E_Pur_T-Sim.x_res.E_Grid_sup_T, 30/Sim.par.delta_T, Sim.par.delta_T/60);
                        P_c=output_average_input(Sim.par.flex.T_c)/Sim.par.delta_T*60;
                        obj.Flex_Sum_res_real.deltaE(e1,e2)  =   P_c(2)/60*Sim.par.T_agg_T-Sim.par.flex.E_T_c(2);
                        obj.Flex_Sum_res_real.deltaE2(e1,e2) =   P_c(3)/60*Sim.par.T_agg_T-Sim.par.flex.E_T_c(3);
                        obj.Flex_Sum_res_real.CT(e1,e2)      =   Sim.x_res.I_CT;
                        obj.Flex_Sum_res_real.ID(e1,e2)      =   Sim.ID;
                        obj.Flex_Sum_res_real.mipgap_100(e1,e2)  =   Sim.mipgap*100;
                        
                    end
            end
            
        end
        
        function obj =save(obj)
        %Function saves Obj. into predefined folder
            cd(obj.folder_store);
            eval(strcat('Sim_',obj.Savename,'=obj;'));
            save(strcat('Sim_',obj.Savename,'.mat'), strcat('Sim_',obj.Savename),'-v7.3');
            cd(obj.folder_sim);   
        end
        
                
        function obj =calc_BESS_aging(obj)
            if obj.N_Base>0
                for i=1:obj.N_Base
                    obj.Base(i)=obj.Base(i).calc_BESSAging;
                end
            end
            if obj.N_Sim>0
                [a,b]=size(obj.Flex_Sum_res.ID);
                obj.Flex_Sum_res.BESS_lifeloss=zeros(a,b);
                obj.Flex_Sum_res.BESS_lifeloss_delta=zeros(a,b);
                for i=1:obj.N_Sim
                    obj.Simulations(i)=obj.Simulations(i).calc_BESSAging;
                    [col,row] = find(obj.Flex_Sum_res.ID==obj.Simulations(i).ID);
                    obj.Flex_Sum_res.BESS_lifeloss(col,row)         =obj.Simulations(i).BESS_lifeloss;
                    obj.Flex_Sum_res.BESS_lifeloss_delta(col,row)   =obj.Simulations(i).BESS_lifeloss-obj.Base.BESS_lifeloss;
                    
                end
            end
        end
        
        function obj =store_res(obj)
            if obj.N_Sim>0
                [a,b]=size(obj.Flex_Sum_res.ID);
                obj.Flex_Sum_res.time=zeros(a,b);
                obj.Flex_Sum_res.mipgap=zeros(a,b);
                for i=1:obj.N_Sim
                    [col,row] = find(obj.Flex_Sum_res.ID==obj.Simulations(i).ID);
                    obj.Flex_Sum_res.time(col,row)         =obj.Simulations(i).time;
                    obj.Flex_Sum_res.mipgap(col,row)       =obj.Simulations(i).mipgap;
                    
                end
            end
        end
        %% Plot Functions
        function obj =plot_flexibilityplan(obj, printname)
            if nargin <=1
               s_print=0; %0 entspricht not print
               printname='test'; 
            else
                s_print=1;
            end
            plotFlexibilityPlan_2dim_only(obj.Flex_Sum_res, s_print, printname, obj.folder_graphs)
        end
        
        function obj =plot_flexibilityplan_real(obj, printname)
            if nargin <=1
               s_print=0; %0 entspricht not print
               printname='test'; 
            else
                s_print=1;
            end
            plotFlexibilityPlan_2dim_only(obj.Flex_Sum_res_real, s_print, printname, obj.folder_graphs)
        end
        
        function obj =plot_BESSaging(obj, printname)
            if nargin <=1
               s_print=0; %0 entspricht not print
               printname='test'; 
            else
                s_print=1;
            end
            plotBESSAging_2dim_only(obj.Flex_Sum_res, s_print, printname, obj.folder_graphs)
        end
        
        function obj =plot_ResultsPlots_mult(obj, index_vect, printname)
            if nargin <=2
               s_print=0; %0 entspricht not print
               printname='test'; 
            else
                s_print=1;
            end
            
            ind=length(index_vect);
            for i=1:ind
                Sim(i)=obj.Simulations(index_vect(i)).returnSim;
                if s_print==1
                    obj.Simulations(index_vect(i)).plot_Schedule(strcat(printname,'#',num2str(index_vect(i))), obj.folder_graphs);
                end
            end
            switch ind
                case 1
                    Machineresultplots(Sim(1));
                case 2
                    Machineresultplots(Sim(1), Sim(2));           
                case 3
                    Machineresultplots(Sim(1), Sim(2), Sim(3));
                case 4
                    Machineresultplots(Sim(1), Sim(2), Sim(3), Sim(4));
                otherwise
                    disp('selection too large or too small')
            end
            
            %%% Falls gespeichert wird
            if s_print==1
                set(gcf,'Units','centimeters','PaperPosition',[0 0 16 10],'PaperPositionMode','manual','PaperSize',[16 10])
                print(gcf,'-r300','-dtiff', '-opengl',strcat(obj.folder_graphs,printname))

            end
        end

        function obj =plot_allSchedules(obj, printname)
            for i=1:obj.N_Sim
                obj.Simulations(i).plot_Schedule(strcat(printname,...
                    '#',num2str(i),...
                    '|P1',num2str(obj.Simulations(i).delta_E1*2),...
                    '|P2',num2str(obj.Simulations(i).delta_E2*2)),...
                    obj.folder_graphs);
                close all
            end
        end
    end
    
end

