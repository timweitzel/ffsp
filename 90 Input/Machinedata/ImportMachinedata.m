function [ep_imt, es_limt, ei_im, eonoff_imt, M, I, J, M_j, M_P, P_im,  A_lim,  SUD_im ] = ImportMachinedata(var)

%% Import Machinedata
%clear all


if var==10
    name=strcat('MachineEnergyConsumption_10min','.xlsx');
elseif var==5
    name=strcat('MachineEnergyConsumption_5min','.xlsx');
else
    name=strcat('MachineEnergyConsumption_v2','.xlsx');
end

filename = name;
Input=readtable(name);

M=max(Input.machine);
J=max(Input.stage);
I=max(Input.product);
Tmax=max(Input.T);


ep_imt=zeros(I,M,Tmax);
es_limt=zeros(I,I,M,Tmax);
ei_im=zeros(I,M);
eonoff_imt=zeros(I,M,Tmax);


A=table2array(Input);
A_num=length(A(:,1));

for a=1:A_num
    m=A(a,1);
    j=A(a,2);
    i=A(a,3);
    l=A(a,4);
    mode=A(a,5);
    t_max=A(a,7);
    %M_j(j,end)=m;
    m_cap=A(a,6);
    
    index=7;
    switch mode
        case 1
            for t=1:1:t_max
                ep_imt(i,m,t)=A(a,index+t);
            end
        case 2
                ei_im(i,m)=A(a,index+1);
        case 3
            for t=1:1:t_max
                if l==i
                    es_limt(l,i,m,t)=0;
                else
                    es_limt(l,i,m,t)=A(a,index+t);
                end
            end
        case 4 
            for t=1:1:t_max
                eonoff_imt(i,m,t)=A(a,index+t);
            end
    end

end

Input2=Input(1:A_num,{'machine','stage'});
B=table2array(Input2);
M_j=zeros(J,1);
M_j_index=ones(J,1);
for b=1:length(B(:,1))
    j=B(b,2);
    if not(isnan(j))
        if isempty(M_j(M_j==B(b,1)))
            M_j(j,M_j_index(j))=B(b,1);
            M_j_index(j)=M_j_index(j)+1;
        end
    end
end

Input3=Input(1:A_num,{'machine','product', 'mode', 'Capacity'});
C=table2array(Input3);
M_P=zeros(M,I);

for b=1:length(C(:,1))
    i=C(b,2);
    m=C(b,1);
    if C(b,3)==1
        M_P(m,i)=C(b,4);
    end
end

% Koeffizientenmatrix der Produktionszeiten
P_im=zeros(I,M);
for m=1:M
    for i=1:I
        a=ep_imt(i,m,:);
        P_im(i,m)=length(a(a>0));
    end
end
clear a m i

% Koeffizientenmatrix der R?stzeiten
A_lim=zeros(I,I,M);
for m=1:M
    for i=1:I
        for l=1:I
            if l==i
                A_lim(l,i,m)=0;
            end      
        a=es_limt(l,i,m,:);
        A_lim(l,i,m)=length(a(a>0));
        end
    end
end


% Koeffizientenmatrix der StartupZeiten
SUD_im=zeros(I,M);
for m=1:M
    for i=1:I    
        a=eonoff_imt(i,m,:);
        SUD_im(i,m)=length(a(a>0));
    end
end

end
