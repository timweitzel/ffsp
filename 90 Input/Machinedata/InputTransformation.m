
%% Drehprozess
input=LP.dreh;
MachineP.m1.dreh.idle=input(825);
MachineP.m3.dreh.idle=input(825);
MachineP.m5.dreh.idle=input(825);
MachineP.m10.dreh.idle=input(825);

MachineP.m1.dreh.prod.E_1min         =LP_1min.dreh.E_in_1min;
MachineP.m1.dreh.prod.P_avg_1min     =LP_1min.dreh.P_in1min;
MachineP.m1.dreh.prod.P_1minAvg_sec  =LP_1min.dreh.P_in_1minAvg_ins;
MachineP.m1.dreh.prod.P_sec          =LP_1min.dreh.P_ins;   

MachineP.m3.dreh.prod.E_3min         =LP_3min.dreh.E_in_3min;
MachineP.m3.dreh.prod.P_avg_3min     =LP_3min.dreh.P_in3min;
MachineP.m3.dreh.prod.P_3minAvg_sec  =LP_3min.dreh.P_in_3minAvg_ins;
MachineP.m3.dreh.prod.P_sec          =LP_3min.dreh.P_ins; 

MachineP.m5.dreh.prod.E_5min         =LP_5min.dreh.E_in_5min;
MachineP.m5.dreh.prod.P_avg_5min     =LP_5min.dreh.P_in5min;
MachineP.m5.dreh.prod.P_5minAvg_sec  =LP_5min.dreh.P_in_5minAvg_ins;
MachineP.m5.dreh.prod.P_sec          =LP_5min.dreh.P_ins;  

MachineP.m10.dreh.prod.E_10min         =LP_10min.dreh.E_in_10min;
MachineP.m10.dreh.prod.P_avg_10min     =LP_10min.dreh.P_in10min;
MachineP.m10.dreh.prod.P_10minAvg_sec  =LP_10min.dreh.P_in_10minAvg_ins;
MachineP.m10.dreh.prod.P_sec          =LP_10min.dreh.P_ins;   


%% Reinigungsprozess
input=LP.rein;
MachineP.m1.rein.idle=input(604)*0.5;
MachineP.m3.rein.idle=input(604)*0.5;
MachineP.m5.rein.idle=input(604)*0.5;
MachineP.m10.rein.idle=input(604)*0.5;

%1min
input=LP.rein(1:3780);
[MachineP.m1.rein.prod.E_1min,MachineP.m1.rein.prod.P_avg_1min,MachineP.m1.rein.prod.P_1minAvg_sec]=averagingOutput (input, 1*60, 1/60/60);
MachineP.m1.rein.prod.P_sec=input;


%3min
input=LP.rein(1:3780);
[MachineP.m3.rein.prod.E_3min,MachineP.m3.rein.prod.P_avg_3min,MachineP.m3.rein.prod.P_3minAvg_sec]=averagingOutput (input, 3*60, 1/60/60);
MachineP.m3.rein.prod.P_sec=input;

% 5min
%Erg�nzung der Input daten mit Idle usage.
input=LP.rein;
input(3795:3900)=1500;
[MachineP.m5.rein.prod.E_5min,MachineP.m5.rein.prod.P_avg_5min,MachineP.m5.rein.prod.P_5minAvg_sec]=averagingOutput (input, 5*60, 1/60/60);
MachineP.m5.rein.prod.P_sec=input;

% 10min
%Erg�nzung der Input daten mit Idle usage.
input=LP.rein;
input(3795:4200)=1500;
[MachineP.m10.rein.prod.E_10min,MachineP.m10.rein.prod.P_avg_10min,MachineP.m10.rein.prod.P_10minAvg_sec]=averagingOutput (input, 10*60, 1/60/60);
MachineP.m10.rein.prod.P_sec=input;

%% Schleifprozess
input=LP.schl;
M_schleif.idle=input(416)*0.5;

% 1min
%Erg�nzung der Input daten mit Idle usage.

input=LP.schl;
MachineP.m1.schl.idle=input(416)*0.5;
MachineP.m3.schl.idle=input(416)*0.5;
MachineP.m5.schl.idle=input(416)*0.5;
MachineP.m10.schl.idle=input(416)*0.5;

input=LP.schl;
input(417:420)=input(416);
[MachineP.m1.schl.prod.E_1min,MachineP.m1.schl.prod.P_avg_1min,MachineP.m1.schl.prod.P_1minAvg_sec]=averagingOutput (input, 1*60, 1/60/60);
MachineP.m1.schl.prod.P_sec=input;


%3min
input(1:70)=M_schleif.idle;
input(1+70:416+70)=LP.schl;
input(417+70:540)=M_schleif.idle;
[MachineP.m3.schl.prod.E_3min,MachineP.m3.schl.prod.P_avg_3min,MachineP.m3.schl.prod.P_3minAvg_sec]=averagingOutput (input, 3*60, 1/60/60);
MachineP.m3.schl.prod.P_sec=input;

% 5min
%Erg�nzung der Input daten mit Idle usage.
versatz=0;
%input(1:90)=M_schleif.idle;
input(1+versatz:416+versatz)=LP.schl;
input(417+versatz:600)=M_schleif.idle;
[MachineP.m5.schl.prod.E_5min,MachineP.m5.schl.prod.P_avg_5min,MachineP.m5.schl.prod.P_5minAvg_sec]=averagingOutput (input, 5*60, 1/60/60);
MachineP.m5.schl.prod.P_sec=input;

figure
plot(MachineP.m5.schl.prod.P_sec)
hold on
plot(MachineP.m5.schl.prod.P_5minAvg_sec)

% 10min
%Erg�nzung der Input daten mit Idle usage.
versatz=0;
%input(1:90)=M_schleif.idle;
input(1+versatz:416+versatz)=LP.schl;
input(417+versatz:600)=M_schleif.idle;
[MachineP.m10.schl.prod.E_10min,MachineP.m10.schl.prod.P_avg_10min,MachineP.m10.schl.prod.P_10minAvg_sec]=averagingOutput (input, 10*60, 1/60/60);
MachineP.m10.schl.prod.P_sec=input;


%% W�rmeprozess
input_warm_total=LP.warm_g(27000:32000);
M_warm.idle=input_warm_total(76);
input_sup=input_warm_total(1:560);
input_sup(561:600)=M_warm.idle;
input_warm=input_warm_total(623:4222);
input_sdn=input_warm_total(4223:4223+600-1);


MachineP.m1.warm.idle     =M_warm.idle;
MachineP.m3.warm.idle     =M_warm.idle;
MachineP.m5.warm.idle     =M_warm.idle;
MachineP.m10.warm.idle    =M_warm.idle;


% 1min
%Erg�nzung der Input daten mit Idle usage.
input=input_warm;
[MachineP.m1.warm.prod.E_1min,MachineP.m1.warm.prod.P_avg_1min,MachineP.m1.warm.prod.P_avg1min_sec]=averagingOutput (input, 1*60, 1/60/60);
MachineP.m1.warm.prod.P_sec=input;


%3min
input=input_warm;
[MachineP.m3.warm.prod.E_3min,MachineP.m3.warm.prod.P_avg_3min,MachineP.m3.warm.prod.P_avg3min_sec]=averagingOutput (input, 3*60, 1/60/60);
MachineP.m3.warm.prod.P_sec=input;

% 5min
%Erg�nzung der Input daten mit Idle usage.
input=input_warm;
versatz=0;
%input(1:90)=M_warm.idle;
%input(1+versatz:416+versatz)=input_warm;
%input(417+versatz:600)=M_warm.idle;
[MachineP.m5.warm.prod.E_5min,MachineP.m5.warm.prod.P_avg_5min,MachineP.m5.warm.prod.P_avg5min_sec]=averagingOutput (input, 5*60, 1/60/60);
MachineP.m5.warm.prod.P_sec=input;

figure
plot(MachineP.m5.warm.prod.P_sec)
hold on
plot(MachineP.m5.warm.prod.P_avg5min_sec)

% 10min
%Erg�nzung der Input daten mit Idle usage.
input=input_warm;
versatz=0;
%input(1:90)=M_warm.idle;
%input(1+versatz:416+versatz)=input_warm;
%input(417+versatz:600)=M_warm.idle;
[MachineP.m10.warm.prod.E_10min,MachineP.m10.warm.prod.P_avg_10min,MachineP.m10.warm.prod.P_avg10min_sec]=averagingOutput (input, 10*60, 1/60/60);
MachineP.m10.warm.prod.P_sec=input;

%% Warm sup sdn
% 1min
%Erg�nzung der Input daten mit Idle usage.
input=input_sdn;
[MachineP.m1.warm.sdn.E_1min,MachineP.m1.warm.sdn.P_avg_1min,MachineP.m1.warm.sdn.P_avg1min_sec]=averagingOutput (input, 1*60, 1/60/60);
MachineP.m1.warm.sdn.P_sec=input;
[MachineP.m3.warm.sdn.E_3min,MachineP.m3.warm.sdn.P_avg_3min,MachineP.m3.warm.sdn.P_avg3min_sec]=averagingOutput (input, 3*60, 1/60/60);
MachineP.m3.warm.sdn.P_sec=input;
[MachineP.m5.warm.sdn.E_5min,MachineP.m5.warm.sdn.P_avg_5min,MachineP.m5.warm.sdn.P_avg5min_sec]=averagingOutput (input, 5*60, 1/60/60);
MachineP.m5.warm.sdn.P_sec=input;
[MachineP.m10.warm.sdn.E_10min,MachineP.m10.warm.sdn.P_avg_10min,MachineP.m10.warm.sdn.P_avg10min_sec]=averagingOutput (input, 10*60, 1/60/60);
MachineP.m10.warm.sdn.P_sec=input;

input=input_sup;
[MachineP.m1.warm.sup.E_1min,MachineP.m1.warm.sup.P_avg_1min,MachineP.m1.warm.sup.P_avg1min_sec]=averagingOutput (input, 1*60, 1/60/60);
MachineP.m1.warm.sup.P_sec=input;
[MachineP.m3.warm.sup.E_3min,MachineP.m3.warm.sup.P_avg_3min,MachineP.m3.warm.sup.P_avg3min_sec]=averagingOutput (input, 3*60, 1/60/60);
MachineP.m3.warm.sup.P_sec=input;
[MachineP.m5.warm.sup.E_5min,MachineP.m5.warm.sup.P_avg_5min,MachineP.m5.warm.sup.P_avg5min_sec]=averagingOutput (input, 5*60, 1/60/60);
MachineP.m5.warm.sup.P_sec=input;
[MachineP.m10.warm.sup.E_10min,MachineP.m10.warm.sup.P_avg_10min,MachineP.m10.warm.sup.P_avg10min_sec]=averagingOutput (input, 10*60, 1/60/60);
MachineP.m10.warm.sup.P_sec=input;

