T=length(LP.dreh);
i=1;
LP.dreh_5min(1)=zeros(1);
n=1;
for t=1:T
    LP.dreh_5min(i)=LP.dreh_5min(i)+LP.dreh(t);
    n=n+1;
    if or(n==5*60,t==T)
        LP.dreh_5min(i)=LP.dreh_5min(i)/60/5;
        i=i+1;
        n=1;
        LP.dreh_5min(i)=0;
    end
end

n=1;
i=1;
for t=1:T
    LP.dreh_5min_ext(t)=LP.dreh_5min(i);
    n=n+1;
    if or(n==5*60,t==T)
        i=i+1;
        n=1;
    end
end