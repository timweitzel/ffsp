

figure

subplot(5,1,1)
hold on
stairs(LP_3min.dreh.P_ins/1000);
stairs(LP_1min.dreh.P_in_1minAvg_ins(31:1830)/1000);
legend('in Seconds', 'in 1 min Average')
xlabel('t in min')
ylabel('P in kW')
set(gca,'XTick',      [0:60:1800])
set(gca,'XTickLabel', [0:60:1800]/60)
hold off

subplot(5,1,2)
hold on
stairs(LP_3min.dreh.P_ins/1000);
stairs(LP_3min.dreh.P_in_3minAvg_ins/1000);
legend('in Seconds', 'in 3 min Average')
xlabel('t in min')
ylabel('P in kW')
set(gca,'XTick',      [0:60:1800])
set(gca,'XTickLabel', [0:60:1800]/60)
hold off

subplot(5,1,3)
hold on
stairs(LP_3min.dreh.P_ins/1000);
stairs(LP_5min.dreh.P_in_5minAvg_ins/1000);
legend('in Seconds', 'in 5 min Average')
xlabel('t in min')
ylabel('P in kW')
set(gca,'XTick',      [0:60:1800])
set(gca,'XTickLabel', [0:60:1800]/60)
hold off

subplot(5,1,4)
hold on
stairs(LP_3min.dreh.P_ins/1000);
stairs(LP_10min.dreh.P_in_10minAvg_ins/1000);
legend('in Seconds', 'in 10 min Average')
xlabel('t in min')
ylabel('P in kW')
set(gca,'XTick',      [0:60:1800])
set(gca,'XTickLabel', [0:60:1800]/60)
hold off

subplot(5,1,5)
hold on
stairs(LP_1min.dreh.P_in_1minAvg_ins(31:1830)/1000);
stairs(LP_3min.dreh.P_in_3minAvg_ins/1000);
stairs(LP_5min.dreh.P_in_5minAvg_ins/1000);
stairs(LP_10min.dreh.P_in_10minAvg_ins/1000);
legend('in 1 min Average','in 3 min Average','in 5 min Average', 'in 10 min Average')
xlabel('t in min')
ylabel('P in kW')
set(gca,'XTick',      [0:60:1800])
set(gca,'XTickLabel', [0:60:1800]/60)
hold off
