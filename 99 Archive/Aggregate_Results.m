N=13;
h_vect=[11:1:12];
H=numel(h_vect);

path='H:\01 Matlab Modelle (weitzel)\80 Simulationen\Simulation-5min__17-04-27_15-21\';

clear res_CT
clear res_deltaE
clear Sim
for n=1:N
    for h=1:H
       name=strcat('Sim_',num2str(h_vect(h)),'_',num2str(n));
       Input=load(strcat(path,name));
       Sim(n,h)= Input.(sprintf(name));
       res_deltaE(n,h)=Sim(n,h).par.flex.eps.delta(Sim(n,h).e);     
       res_CT(n,h)=Sim(n,h).I_CT;
    end
end