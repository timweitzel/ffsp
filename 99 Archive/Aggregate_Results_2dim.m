function [Sim]=Aggregate_Results_2dim(h_vect, path)
%h_vect=[12:1:12];
folderold=pwd;
H=numel(h_vect);

%load parameters
cd(path)
name=strcat('Sim_',num2str(h_vect(1)),'_1');
Input=load(name);
Input2=Input.(sprintf(name)); %Sim_1_1
N=Input2.par.flex.eps.num;
N1=Input2.par.flex.eps.num1;
N2=Input2.par.flex.eps.num2;


clear res_CT
clear res_deltaE
clear Sim
n=1;
for n1=1:N1
    for n2=1:N2
        for h=1:H
           name=strcat('Sim_',num2str(h_vect(h)),'_',num2str(n));
           Input=load(strcat(path,'/',name));
           Sim(n1,n2,h)= Input.(sprintf(name));
           res.deltaE(n1,n2,h)=Sim(n1,n2,h).par.flex.eps.delta1(Sim(n1,n2,h).e1);
           res.deltaE2(n1,n2,h)=Sim(n1,n2,h).par.flex.eps.delta2(Sim(n1,n2,h).e2); 
           res.CT(n1,n2,h)=Sim(n1,n2,h).I_CT;
        end
        n=n+1;
    end
end

save('Sim','Sim','res','-v7.3')
cd(folderold)
end