handles=findall(0,'type','figure');
handles.delete;
clear all
tges=datetime('now');



%% Options for Optimization
opt.INCL_Pgrid_neg=0; %=1 falls Einspeisung zul?ssig

opt.RecedingHorizon=0; %=1 falls einige Werte bereits durch fr?here Optimierung festgelegt sind
opt.FlexCalc=0;

opt.cplex.DisplayIter       =1;% 1-Iteration werden angezeigt, 0- werden nicht angezeigt %

opt.cplex.TimeCap           =420*60;%5*60min TimeCap
opt.cplex.SolTolerance      =0.001;%0.005;


%% Parameter settings

par.obj_weights.w_Cost=1/1000;   % Gewichtung der Kostenkomponente
par.obj_weights.w_MS=1;

par.flex.eps.delta=[0];
par.flex.eps.num=numel(par.flex.eps.delta);

% Time periods investigated
par.delta_T         = 5; %min
par.Time.Start_h    = 6;
par.Time.Ende_h     = 20;%Ende zur vollen STunde 22->21:59
par.Time.Sim_h      = par.Time.Ende_h -par.Time.Start_h; %Anzahl der Stunden die simuliert werden
par.T               = par.Time.Sim_h /(par.delta_T/60);
par.T_1             = par.T + 1; %Endzeit f?r Statusgr??en

par.T_S_inh     =6; %STartzeit der Produktion in Uhrzeit
par.T_S         =  (par.T_S_inh-par.Time.Start_h)/(par.delta_T/60);%(7-1)/(par.delta_T/60); %Produktionsstart
par.T_E_inh     =20; %Endzeit der Produktion in Uhrzeit
par.T_E         =  (par.T_E_inh-par.Time.Start_h)/(par.delta_T/60);%20/(par.delta_T/60); %Produktionsende



%Energy Storage
par.ESS.sigma       =0;
par.ESS.RTE         =0.95;
par.ESS.eta_c       =sqrt(par.ESS.RTE);
par.ESS.eta_d       =sqrt(par.ESS.RTE);
par.ESS.E_max       =0;
par.ESS.E_min       =0;
par.ESS.P_c_max     =100;
par.ESS.P_d_max     =100;
par.ESS.P_c_min     =1;
par.ESS.P_d_min     =1;
par.ESS.E_0         =0.5 * par.ESS.E_max;

%energy prices
% Input definition
par.DAM.year=2015;
par.DAM.month=9;%2;
par.DAM.day=15;
In.DAM.p_t =      ImportPricedata(par.delta_T,par.DAM.day,par.DAM.month,par.DAM.year,1);
In.DAM.p_t =      In.DAM.p_t((par.Time.Start_h)*60/par.delta_T+1:(par.Time.Start_h+par.Time.Sim_h)*60/par.delta_T);


%energy production in kW par.delta_T
par.PV.year         =   par.DAM.year;
par.PV.month        =   par.DAM.month;
par.PV.day          =   par.DAM.day;
par.PV.duration     =   1; %1day
par.PV.stretch      =   1/4;
In.E_PV_t           =   ImportPVdata( par.delta_T, par.PV.day, par.PV.month, par.PV.year,1, par.PV.stretch )*par.delta_T/60; %Import und transfer in kWh
In.E_PV_t           =   In.E_PV_t((par.Time.Start_h)*60/par.delta_T+1:(par.Time.Start_h+par.Time.Sim_h)*60/par.delta_T); %Rduktion auf betrachteten Zeitraum


%energy consumption per period in kwh per par.delta_T
[In.ep_imt, In.es_limt, In.ei_im, In.esud_imt, par.M, par.I, par.J, In.M_J , par.M_P ,In.P_im,  In.A_lim,  In.SUD_im ] = ImportMachinedata(par.delta_T);


%% Ordermatrix

In.D.E_k    =[2 1]; %product type per order
In.D.D_k    =[24 20];%[24 20]; %amount of product per order
In.D.d_k    =[par.T, par.T]; %finish time
par.K=numel(In.D.E_k); %orders

In.D.r_kt       =   zeros(par.K,par.T);
In.D.r_kt(1,1)  =   In.D.D_k(1); %Definiert fr?hesten Beginn der Produktion
In.D.r_kt(2,1)  =   In.D.D_k(2); %Definiert fr?hesten Beginn der Produktion
%In.D.r_kt(3,1)  =   In.D.D_k(3); %Definiert fr?hesten Beginn der Produktion

%% Flex parameters (Basis not for calculation)

par.T_agg_T=60; % aggregierte Zeitintervalle in min
par.T_agg_T_mult= par.T_agg_T / par.delta_T; %Anzahl der zu aggregierenden Zeitintervalle
par.flex.T_c_inh_vect=8;
par.flex.T_c_inh_vect_num=numel(par.flex.T_c_inh_vect);

par.T_RH_inh=6; %1h vor flexibilisierungsstunde
par.T_RH = (par.T_RH_inh+1-par.Time.Start_h)/(par.delta_T/60)-par.T_S;
par.flex.T_c_inh    =8;
par.flex.T_c        =(par.flex.T_c_inh-par.Time.Start_h)/(par.delta_T/60)+1;
par.flex.E_T_c      =0;

par.flex.delta_E            =0; % Abweichung in %


%% Folder location
folderold=pwd;
folder1='/Users/Timm/Documents/MATLABgit/Results/'; %for Apple
%folder1=strcat('C:\Users\weitzel\Desktop\local\'); % Herakeles
cd(folder1);
foldername=strcat('Simulation-ESS-',num2str(par.ESS.E_max) ,'-',num2str(par.delta_T),'min_', datestr(now,'_yy-mm-dd_HH-MM'));
mkdir(foldername);
folder=strcat(folder1,foldername);
cd(folderold);

%% Initial Optimization
[PROBLEM, x_var]=   FFSP_OM(par, In, opt);
[x_res, x_var]=     FFSP_OMSolverSub(par, In, opt, x_var, PROBLEM);
clearvars PROBLEM

cd(folder);
Sim=x_res;
Sim.var=x_var;
Sim.par=par;
Sim.In=In;
Sim.opt=opt;
name=strcat('Sim_basic');
eval(sprintf(strcat(name,' =Sim;')));
save(strcat(name,'.mat'),name);
cd(folderold);

%% Flex Variation

%%Parameter definition
par.flex.T_c_inh_vect   = [10:1:10];%14
par.flex.eps.delta1     = [-50:10:0];%200
par.flex.eps.delta2     = [-50:10:0];%200
opt.cplex.DisplayIter   = 0;% 1-Iteration werden angezeigt, 0- werden nicht angezeigt %
opt.cplex.TimeCap       = 420*60;%5*60min TimeCap
opt.RecedingHorizon     = 1;
opt.FlexCalc            = 1;

par.T_agg_T             =30; % aggregierte Zeitintervalle in min
par.T_agg_T_mult        = par.T_agg_T / par.delta_T; %Anzahl der zu aggregierenden Zeitintervalle

clear Sim
Sim=FFSP_CMFlexgen(folder, folderold, Sim_basic, par, opt, In);
tges2=datetime('now')
tdelta=tges2-tges