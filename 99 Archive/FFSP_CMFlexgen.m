function [Sim]=FFSP_CMFlexgen(folder, folderold, Sim_basic, par, opt, In)
%% Flexibilit?tsma?nahmen

tges=datetime('now');

par.flex.T_c_inh_vect_num=numel(par.flex.T_c_inh_vect);


for fl_int=1:par.flex.T_c_inh_vect_num
    %Definition receding horizon parameter
    par.T_RH_inh=par.flex.T_c_inh_vect(fl_int); %1h vor flexibilisierungsstunde
    par.T_RH_vorFlex=par.T_agg_T/par.delta_T;
    par.T_RH = (par.T_RH_inh-(par.Time.Start_h))/(par.delta_T/60)-par.T_S-par.T_RH_vorFlex;
    
    %Auswahl neuer Basis
    In.RecedingHorizon=     Sim_basic; %Input for receding horizon
    
    %% Problem is generated for faster variant calculation
    [PROBLEM, x_var]=FFSP_OM(par, In, opt);
    
    %Bestimmung zu reduzierenden Stunde
    output_sum     =averagingOutput (Sim_basic.E_Pur_T/par.delta_T*60, 30/par.delta_T, par.delta_T/60); %Output wird hier auf stundenniveaus aggregiert um zielgr??e zu definieren
    par.flex.T_c_inh    =par.flex.T_c_inh_vect(fl_int);
    par.flex.T_c =zeros(3,1);
    par.flex.T_c(2)        =(par.flex.T_c_inh-par.Time.Start_h)/(par.delta_T/60)+1; % target value
    par.flex.T_c(1)        = par.flex.T_c(2)-par.T_agg_T_mult; % preperiod
    par.flex.T_c(3)        = par.flex.T_c(2)+par.T_agg_T_mult; % following period
    par.flex.E_T_c(1)      =output_sum(par.flex.T_c_inh*60/par.T_agg_T-par.Time.Start_h*60/par.T_agg_T+0); % preperiod
    par.flex.E_T_c(2)      =output_sum(par.flex.T_c_inh*60/par.T_agg_T-par.Time.Start_h*60/par.T_agg_T+1); % target value
    par.flex.E_T_c(3)      =output_sum(par.flex.T_c_inh*60/par.T_agg_T-par.Time.Start_h*60/par.T_agg_T+2); % following period
    par.flex.T_c_fix_inh    =0;
    for i=1:par.flex.T_c_fix_inh
        par.flex.E_T_c_fix(i)       =output_sum(par.flex.T_c_inh-par.Time.Start_h+1+i);
        par.flex.T_c_fix_start(i)   =(par.flex.T_c_inh+i-par.Time.Start_h)/(par.delta_T/60)+1;
    end
    
    par.flex.eps.num1=numel(par.flex.eps.delta1);
    par.flex.eps.num2=numel(par.flex.eps.delta2);
    par.flex.eps.num= par.flex.eps.num1* par.flex.eps.num2;
    
    et=1;
    for e1=1:par.flex.eps.num1
        for e2=1:par.flex.eps.num2
            par.flex.delta_E                =    par.flex.eps.delta1(e1);
            par.flex.delta_E2                =    par.flex.eps.delta2(e2);
            par.flex.E_T_c_target(1)           =    max(0,par.flex.E_T_c(1)); %falls E_T_C bereits null werden
            par.flex.E_T_c_target(2)           =    max(0,par.flex.E_T_c(2)+par.flex.delta_E); %falls E_T_C bereits null werden
            par.flex.E_T_c_target(3)           =    max(0,par.flex.E_T_c(3)+par.flex.eps.delta2(e2)); %falls E_T_C bereits null werden
            if ((1+par.flex.eps.delta1(e1))==1) && ((1+par.flex.eps.delta2(e2))==1)
                res=Sim_basic;
            else
                res=FFSP_OMSolverSub(par, In, opt, x_var, PROBLEM);
            end
            
            res_CT(fl_int,et)                =res.I_CT;
            res_deltaE(fl_int,et)            =par.flex.eps.delta1(e1);
            res_targetE_inkWh(fl_int,et)     =par.flex.E_T_c_target(2);
            S=sprintf(strcat('FlexHour: '    , num2str(fl_int),'/',num2str(par.flex.T_c_inh_vect_num),...
                ' -- DeltaE: '   , num2str(et),'/',num2str(par.flex.eps.num),...
                ' -- t_iter: '   , num2str(res.time),'in s', ...
                ' -- gap: '      , num2str( ceil(res.mipgap*1000)/10 ), ' %%',...
                ' -- status: '   , res.status),...
                ' -- t_ges: '    , datestr(datetime('now')-tges,'dd:HH:MM:SS'));
            disp(S);
            
            %Save file
            cd(folder);
            Sim=res;
            Sim.var=x_var;
            Sim.par=par;
            Sim.In=In;
            Sim.opt=opt;
            Sim.e1=e1;
            Sim.E1=par.flex.eps.num1;
            Sim.e2=e2;
            Sim.E2=par.flex.eps.num2;
            Sim.e=et;
            Sim.fl_int=fl_int;
            name=strcat('Sim','_', num2str(par.flex.T_c_inh_vect(fl_int)), '_', num2str(et));
            eval(sprintf(strcat(name,' =Sim;')));
            save(strcat(name,'.mat'),name);
            cd(folderold);
            et=et+1;
            clearvars -except e1 e2 par opt In x_var Sim_basic tges fl_int et PROBLEM foldername folderold folder h_vect
        end
    end
    
end
clear Sim
clear res_CT
clear res_deltaE2 res_deltaE
[Sim]=Aggregate_Results_2dim(par.flex.T_c_inh_vect, folder);
end
