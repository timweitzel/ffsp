

par=Sim(6,6).par;

%% plots
figure()

hold off;
hold all;
grid on
x_res=Sim(6,6);
[output_sum, output_average_P, output_average_input1]=averagingOutput (x_res.E_Pur_T/par.delta_T*60, 30/par.delta_T, par.delta_T/60);
stairs(output_average_input1, '-');
maxY=max(x_res.E_Pur_T/par.delta_T*60);
x_res=Sim(3,3);
[output_sum, output_average_P, output_average_input2]=averagingOutput (x_res.E_Pur_T/par.delta_T*60, 30/par.delta_T, par.delta_T/60);
output_average_input2(5*60/par.delta_T:1:6*60/par.delta_T)=output_average_input1(5*60/par.delta_T:1:6*60/par.delta_T);
stairs(output_average_input2, '-');


%ylim([0 Q_max_S]);
xlabel('time in h');
ylabel('30 min-\0 P in kW')
xlim([1 par.T]);
set(gca,'XTick',      [1:60/par.delta_T:par.T+60/par.delta_T+1])
set(gca,'XTickLabel', [par.Time.Start_h:1:par.Time.Ende_h+1])
ylim([0 maxY]);

h=legend('Baseline', 'DR of \Delta P_{DRP}=-60kW at 10:00-10:30 and 10:30-11:00');
clear max

set(gcf,'Units','centimeters','PaperPosition',[0 0 8 4],'PaperPositionMode','manual','PaperSize',[8 4])
print(gcf,'-r300','-dtiff', '-opengl','/Users/Timm/Dropbox/Promotion/12 Production System Flexibility Provision/01 Grafiken/54_Demand_Response_exemplary_graph')


%%
figure

    hold on
    grid on
    name={};
    value=res.deltaE(3,3,1)/30*60;
    plot(res.deltaE2(3,3,1)/30*60,res.CT(3,3,1),'-o')
    name=[name {strcat('\Delta P_{DRP,n=1}=',num2str(value),'kW')}];
    
    value=res.deltaE(6,6,1)/30*60;
    plot(res.deltaE2(6,6,1)/30*60,res.CT(6,6,1),'-o')
    name=[name {strcat('\Delta P_{DRP,n=1}=',num2str(value),'kW')}];
    
    xlim([min(res.deltaE2(1,:)/30*60)-10 max(res.deltaE2(1,:)/30*60)+10]);
    ylim([950 1100]);
 
    %ylim([min(res.CT(n1,:,i))*0.95 min(res.CT(n1,:,i))*1.2]);
    xl=xlabel('\Delta P_{DRP,n=2} [in kW]');
    yl=ylabel(strcat('TWCT [in min]'));
    set(gca,'YTick',      [950:25:1100])
    %title(strcat(num2str(N_h(i)),':00-',num2str(N_h(i)+1),':00'));
    legend(name, 'Location','northeast')
    
    xl.FontSize=12;
    yl.FontSize=12;
    
    set(gcf,'Units','centimeters','PaperPosition',[0 0 8 5],'PaperPositionMode','manual','PaperSize',[10 5])
    print(gcf,'-r300','-dtiff', '-opengl','/Users/Timm/Dropbox/Promotion/12 Production System Flexibility Provision/01 Grafiken/54 Fexibility Provision example')
