function plotFlexPlan(res, par)
%%res(I,dFE) is a struct with time intervals I and Flexibility deltas dFE

dFE_num=numel(res(1,:));

for i=1:dFE_num-1
    MS(i)       =   res(1,i).I_CT;
    E_cons(i)   =   sum(res(1,i).E_Pur_T);
    delta_E(i)  =   res(1,i).delta_E;
    E_cons_T(i,:) =   res(1,i).E_Pur_T;
end

figure;
subplot(2,1,1);
hold all;
grid on;
plot(delta_E,MS);
ylabel('Total completion time');
xlabel('delta_E');

figure()
hold off;
subplot(2,1,1);
hold all;
grid on
name={};
for i=1:dFE_num-1
    stairs(E_cons_T(i,:), '-');
    name=[name {strcat('Delta_E in % ', num2str(delta_E(i)*100))}];
end
%ylim([0 Q_max_S]);
legend(name);
xlabel('time');
ylabel('in kWh')
set(gca,'XTick',      [1:60/par.delta_T:par.T+60/par.delta_T+1])
set(gca,'XTickLabel', [par.Time.Start_h:1:par.Time.Ende_h+1])

hold off;
subplot(2,1,2);
hold all;
grid on
name={};
for i=1:dFE_num-1
    [A, B, C]=averagingOutput (E_cons_T(i,:), 60/par.delta_T, 0.5);
    stairs(C, '-');
    name=[name {strcat('Delta_E in % ', num2str(delta_E(i)*100))}];
end
%ylim([0 Q_max_S]);
legend(name);
xlabel('time');
ylabel('in kWh')
set(gca,'XTick',      [1:60/par.delta_T:par.T+60/par.delta_T+1])
set(gca,'XTickLabel', [par.Time.Start_h:1:par.Time.Ende_h+1])



end