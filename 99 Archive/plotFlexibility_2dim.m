function plotFlexibility_2dim(Sim, res, s_print, printname, N_h)


N_h=[10:1:10];
n_h=numel(N_h);

x_res=Sim(6,6);
x_res2=Sim(3,3);
par=Sim(1,1,1).par;
opt=Sim(1,1,1).opt;
[N1,N2,H]=size(res.deltaE);

[output_sum, output_average_P, output_average_input]=averagingOutput (x_res.E_Pur_T/par.delta_T*60, 30/par.delta_T, par.delta_T/60);  %Output wird hier auf stundenniveaus aggregiert um zielgr??e zu definieren
[output_sum2, output_average_P2, output_average_input2] =averagingOutput (x_res2.E_Pur_T/par.delta_T*60, 30/par.delta_T, par.delta_T/60); 
output_sum(par.flex.T_c_inh-par.Time.Start_h+1)
output_sum2(par.flex.T_c_inh-par.Time.Start_h+1)

%% MAchineplot und excel save



%% plots
figure()
hold off;
subplot(2,1,1);
hold all;
grid on

maxY=0;
stairs(x_res.E_Pur_T/par.delta_T*60, '-');
stairs(x_res2.E_Pur_T/par.delta_T*60, '-');
maxY=max(maxY,max(x_res.E_Pur_T/par.delta_T*60));
%ylim([0 Q_max_S]);
xlabel('time');
ylabel('in kW')
set(gca,'XTick',      [1:60/par.delta_T:par.T+60/par.delta_T+1])
set(gca,'XTickLabel', [par.Time.Start_h:1:par.Time.Ende_h+1])
ylim([0 maxY]);
xlim([1 par.T]);
clear max

hold off;
subplot(2,1,2);
hold all;
grid on
% for n1=1:1:N1
%     for n2=1:1:N2
%         x_res=Sim(n1,n2,n_h);
%         [output_sum, output_average_P, output_average_input]=averagingOutput (x_res.E_Pur_T/par.delta_T*60, 30/par.delta_T, par.delta_T/60);
%         stairs(output_average_input, '-');
%     end
% end
stairs(output_average_input, '-');
stairs(output_average_input2, '-');
maxY=max(maxY,max(x_res.E_Pur_T));
%ylim([0 Q_max_S]);
xlabel('time');
ylabel('in kW')
set(gca,'XTick',      [1:60/par.delta_T:par.T+60/par.delta_T+1])
set(gca,'XTickLabel', [par.Time.Start_h:1:par.Time.Ende_h+1])
%ylim([0 maxY]);
clear max

if s_print ==1
    set(gcf,'Units','centimeters','PaperPosition',[0 0 8 5],'PaperPositionMode','manual','PaperSize',[8 5])
    print(gcf,'-r300','-dtiff', '-opengl','/Users/Timm/Dropbox/Promotion/12 Paper#3 - Production System Flexibility Provision/01a Grafiken revised/Demand_Response_graph')
end

% plot objective curve
figure

for i=1:1:H
    subplot(1,ceil(H/2),i)
    hold on
    grid on
    name={};
    for n1=1:N1
        value=res.deltaE(n1,1,i)/30*60;
        plot(res.deltaE2(n1,:,i)/30*60,res.CT(n1,:,i),'-o')
        xlim([min(res.deltaE2(n1,:,i)/30*60)-10 max(res.deltaE2(n1,:,i)/30*60)+10]);
        ylim([950 1100]);
        name=[name {strcat('\Delta P_{DRP,n=1}=',num2str(value),'kW')}];
        %ylim([min(res.CT(n1,:,i))*0.95 min(res.CT(n1,:,i))*1.2]);
    end
    xl=xlabel('\Delta P_{DRP,n=2} [in kW]');
    yl=ylabel(strcat('TWCT [in min]'));
    set(gca,'YTick',      [950:25:1100])
    %title(strcat(num2str(N_h(i)),':00-',num2str(N_h(i)+1),':00'));
    legend(name, 'Location','northeast')

    xl.FontSize=12;
    yl.FontSize=12;

end

if s_print ==1
    set(gcf,'Units','centimeters','PaperPosition',[0 0 8 5],'PaperPositionMode','manual','PaperSize',[10 5])
    print(gcf,'-r300','-dtiff', '-opengl',strcat('/Users/Timm/Dropbox/Promotion/12 Paper#3 - Production System Flexibility Provision/01a Grafiken revised/Fexibility Provision',printname))
end
end

