function plotMachineplan(M,T, delta_T, Modes, Machineplan, T_Start_h, In, par)
% N: Number of machines. every machine has 3 states: production, idle and
% setup
% T: number of timeslots
% Machineplan is a matrix with dimension M*Modes x T

%% TEstdaten
%clear all;

for m=1:par.M
    for j=1:par.J
        for n=1:numel(In.M_J(j,:))
            if m==In.M_J(j,n)
                J_M(m)=j;
            end
        end
    end
end

% M=2;
% T=5;
% Modes=3;
% I=1;
% Machineplan = [1,1,1,0,0,;0,0,0,1,1,;0,0,0,0,0;0,0,0,1,1,;1,1,1,0,0;0,0,0,0,0];

color=[ 236 146 13; 12 96 244;  0 204 0; 51 153 255; 84 84 84]/255;
    

N=M*Modes;

%%Code
plotSol = 0;
smallScale = true;
% if nargin > 4
%     plotSol = 1;
%     xsol = varargin{1};
%     if nargin > 5
%         smallScale = varargin{2};
%     end
% end

nPtotal = N*T;%length(PurchaseYears);
PurchaseYears=[];
for n=1:(N*Modes) 
    PurchaseYears=[PurchaseYears, 1:1:T];
end

% Create a figure
if smallScale
    figure('Position',[20 100 700+25*T 100+25*N],...
        'Color','w','Units','Pixels')
else
    figure('Position',[20 100 700 700],...
        'Color','w','Units','Pixels')
end
    
% Create an axis
ax = gca;
% Set the axes to fill the entire figure
set(ax,'Position',[0 0 1 1]);
% remove the axis ticks
axis off

hold on

% Define x- and y-spacing for the timeslots
x=zeros(T+2,1);
x(1)=0.06;
x(2:T+2) = linspace(0.16,0.98,T+1);
xwidth = x(3)-x(2);
xl = 0.02;
if smallScale
    yt = 0.98;
    y = linspace(0.92,0.02,N+1);
    %y = linspace(0.84,0.02,N+1);
    %y2= linspace(0.84/I/Modes,0.02,N+1);
    ywidth0 = y(1)-y(2);
    ywidth = y(2)-y(3);
else
    yt = 0.94;
    y = linspace(0.92,0.02,N+1);
    ywidth0 = y(1)-y(2);
    ywidth = y(2)-y(3);
end

% Plot the vertical timeslot lines
xyear = NaN(3*(T+1)+2,1);
xyear(1:3:end) = x;
xyear(2:3:end) = x;
yyear = NaN(3*(T+1)+2,1);
yyear(1:3:end) = yt;
yyear(2:3:end) = y(end);
plot(xyear,yyear,'k--')
if smallScale
    plot([xl xl],[y(1) y(end)],'k')
end

% if small scale
if smallScale
    % Plot the horizontal machine lines
    xsec = NaN(3*(N+1),1);
    xsec(1:3:end) = xl;
    xsec(2:3:end) = x(end);
    ysec = NaN(3*(N+1),1);
    ysec(1:3:end) = y;
    ysec(2:3:end) = y;
    plot(xsec,ysec,'k')
    plot([x(1) x(end)], [yt yt],'k')
    
    % Add textbox to label each timeslot
    for j=1:T*delta_T/60
        ar = annotation('rectangle','Position',[x((j-1)*60/delta_T+2) y(1) xwidth*60/delta_T yt-y(1)],...
           'FaceColor', [0.97 0.97 0.97]);
        annotation('textbox','Position',[x((j-1)*60/delta_T+2) y(1) xwidth*60/delta_T yt-y(1)],...
            'String', [ '' num2str(j+T_Start_h-1)],...
            'VerticalAlignment','Middle', 'HorizontalAlignment','Center',...
            'EdgeColor','none');
    end

    % Add textbox to label each machine
    for m=1:M
       ar = annotation('rectangle','Position',[xl y((m-1)*Modes+Modes+1) x(1)-xl ywidth*Modes],...
           'FaceColor', [0.95 0.95 0.95]);
        annotation('textbox','Position',[xl y((m-1)*Modes+Modes+1) x(1)-xl ywidth*Modes],...
           'String',['M_' num2str(m) char(10) ],...
            'VerticalAlignment','Middle', 'HorizontalAlignment','Center',...
            'EdgeColor','none');
    end
    
    % Add textbox to label each stage
    for m=1:M
        for j=1:Modes
            switch j
                case 1 
                    stagename='Production';
                case 2 
                    stagename='Idle';
                case 3 
                    stagename='Setup';
                case 4 
                    stagename='Startup/Shutdown';
                case 5 
                    stagename='Offline';
            end
            
                annotation('textbox','Position',[x(1) y((m-1)*Modes+j+1) x(1)-xl ywidth],...
               'String',['' stagename ],...
                'VerticalAlignment','Middle', 'HorizontalAlignment','left',...
                'EdgeColor','none');
        end
    end

    % Plot the Machineplan

    % Start with the first machine on the first line
    %alternator=1;
    for m=1:M
        alternator=J_M(m);
        for j=1:Modes
            for t=1:T              
                if Machineplan ((m-1)*Modes+j,t)==1
                    ar = annotation('rectangle','Position',[x(t+1) y((m-1)*Modes+j+1) x(t+2)-x(t+1) ywidth],...
                    'FaceColor', color(alternator,:));
                end
            end
        end
%         if alternator==1
%             alternator=2;
%         elseif alternator==2;
%             alternator=1;
%         end
    end
    
else % if large scale example
    % Plot the horizontal bond lines
    idx = xsol>1e-2;
    nsol = nnz(idx);
    xsec = NaN(3*nsol,1);
    xsec(1:3:end) = x(PurchaseYears(idx));
    xsec(2:3:end) = x(PurchaseYears(idx)+1);
    ysec = NaN(3*nsol,1);
    idx0 = idx(1:T);
    nsol0 = nnz(idx0);
    ysec(1:3:3*nsol0) = y(1);
    ysec(2:3:3*nsol0) = y(1);
    ysec(3*nsol0+1:3:end) = y(find(idx(T+1:end))+1);
    ysec(3*nsol0+2:3:end) = y(find(idx(T+1:end))+1);
    plot(xsec,ysec,'r','LineWidth',2)
    % Add textbox to label the solution bonds
    tidx = find([sum(idx0);idx(T+1:end)]);
    for i=1:nnz(tidx)
       annotation('textbox','Position',[xl y(tidx(i)) x(1)-xl ywidth],...
           'String',['B_{' num2str(tidx(i)-1) '}'],...
            'VerticalAlignment','Middle', 'HorizontalAlignment','Center',...
            'EdgeColor','none');
    end
    % Add textbox to label each year
    annotation('textbox','Position',[0 yt x(1) 0.98-yt],...
        'String', 'Year ',...
        'VerticalAlignment','Middle', 'HorizontalAlignment','Right',...
        'EdgeColor','none');
    for j=1:T
        annotation('textbox','Position',[x(j) yt xwidth 0.98-yt],...
            'String', num2str(j), 'FontSize', 8,...
            'VerticalAlignment','Middle', 'HorizontalAlignment','Center',...
            'EdgeColor','none');
    end
end

hold off