close all
%plotFlexibility_2dim(Sim50, res50, 1, 'ESS50' , 10)
% plotFlexibility_2dim(Sim50, res50, 1, 'ESS50' , 10)
plotFlexibility_2dim(Sim, res2, 0, 'ESS0' , 10)

% Machineresultplots(Sim50(6,6), Sim50(1,1))
% set(gcf,'Units','centimeters','PaperPosition',[0 0 16 10],'PaperPositionMode','manual','PaperSize',[16 10])
% print(gcf,'-r300','-dtiff', '-opengl','/Users/Timm/Dropbox/Promotion/12 Production System Flexibility Provision/01 Grafiken/63_EnergyDiagram_comp')
% 
% plotMachineplan_production(Sim50(6,6))
% set(gcf,'Units','centimeters','PaperPosition',[0 0 10 4],'PaperPositionMode','manual','PaperSize',[10 4])
% print(gcf,'-r300','-dtiff', '-opengl','/Users/Timm/Dropbox/Promotion/12 Production System Flexibility Provision/01 Grafiken/63_Machineplan_alternativ_ESS_50')
% 
% plotMachineplan_production(Sim50(1,1))
% set(gcf,'Units','centimeters','PaperPosition',[0 0 10 4],'PaperPositionMode','manual','PaperSize',[10 4])
% print(gcf,'-r300','-dtiff', '-opengl','/Users/Timm/Dropbox/Promotion/12 Production System Flexibility Provision/01 Grafiken/63_Machineplan_alternativ_ESS_0')

%% Chapter 6.2 Results without Energy Storage
% 
% Machineresultplots(Sim(6,6), Sim(4,1))
% set(gcf,'Units','centimeters','PaperPosition',[0 0 16 10],'PaperPositionMode','manual','PaperSize',[16 10])
% print(gcf,'-r300','-dtiff', '-opengl','/Users/Timm/Dropbox/Promotion/12 Production System Flexibility Provision/01 Grafiken/62_EnergyDiagram_comp')
% 
% plotMachineplan_production(Sim(6,6))
% set(gcf,'Units','centimeters','PaperPosition',[0 0 10 4],'PaperPositionMode','manual','PaperSize',[10 4])
% print(gcf,'-r300','-dtiff', '-opengl','/Users/Timm/Dropbox/Promotion/12 Production System Flexibility Provision/01 Grafiken/62_Machineplan_basic_v2')
% 
% plotMachineplan_production(Sim(4,1))
% set(gcf,'Units','centimeters','PaperPosition',[0 0 10 4],'PaperPositionMode','manual','PaperSize',[10 4])
% print(gcf,'-r300','-dtiff', '-opengl','/Users/Timm/Dropbox/Promotion/12 Production System Flexibility Provision/01 Grafiken/62_Machineplan_adapted_v2')
% 

% 
% 
% for i=1:6
%     for j=1:6
%         plotMachineplan_production(Sim(i,j))
%     end
% end