
i=12;
SUP60.Simulations(i).mipgap
SUP60.Simulations(i).F
SUP60.Simulations(i).opt.cplex.DisplayIter=1;
SUP60.Simulations(i).opt.cplex.TimeCap=12000;
SUP60.Simulations(i)= SUP60.Simulations(i).solve;
SUP60.Simulations(i).F



i=21;
SUP60.Simulations(i).mipgap
SUP60.Simulations(i).F
SUP60.Simulations(i).opt.cplex.DisplayIter=1;
SUP60.Simulations(i).opt.cplex.TimeCap=12000;
SUP60.Simulations(i)= SUP60.Simulations(i).solve;
SUP60.Simulations(i).F

SUP60.save

%neue Simulations SUP0

try
    %%Parameter definition
    par.T_RH_vorFlex_inmin  = 5;
    SUP0=SUP;
    SUP0=SUP0.Add_flex(par, In, opt);
    SUP0=SUP0.FlexSim;
    SUP0.Savename='Sim0_SensitivityAnalysis_RH5min';
    SUP0.save
catch
    disp('Fehler')
end