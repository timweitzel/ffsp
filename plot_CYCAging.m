close all

figure
CYC_Depth=[1:1:100];
[S,N]=AlterungskurveBESS_cyc(CYC_Depth,4);
hold on
grid on
yyaxis left
plot(CYC_Depth, S*100*100);
xl=xlabel('DOD [in %]');
ylabel('Damage per 100 cycles [in %]');

yyaxis right
plot(CYC_Depth, N);
ylabel('Maximum number of cycles');
ylim([0 100000]);

set(gcf,'Units','centimeters','PaperPosition',[0 0 10 4],'PaperPositionMode','manual','PaperSize',[10 4])
print(gcf,'-r300','-dtiff', '-opengl','/Users/Timm/Dropbox/Plots/AgingModel')
    
%     yyaxis left
%     stairs(x_res.P_ESScrg_T-x_res.P_ESSdcrg_T);
%     ylabel('in kW');
%     ylim([-100 100]);
%     
%     legend('E^{ESS} in kWh', 'P^{ESS} in kW');
%     
%     xlim([1 par.T])
%     set(gca,'XTick',      [1:60/par.delta_T:par.T+1])
%     set(gca,'XTickLabel', [par.Time.Start_h:1:par.Time.Ende_h])