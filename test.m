SUP_changed.par.ESS.E_max=50;
SUP_changed.par_flex.ESS.E_max=50;
SUP_changed.Base.par.ESS.E_max=50;

P_ESSdcrg_T=SUP_changed.Simulations(1).x_res.P_ESSdcrg_T;
P_ESScrg_T=SUP_changed.Simulations(1).x_res.P_ESScrg_T;
P_ESScrg_T(40:80)=0;
P_ESSdcrg_T(40:80)=0;
E_ESSstore_T_1=zeros(169,1);
S_ESSdcrg_T=P_ESSdcrg_T*0;
S_ESSdcrg_T(P_ESSdcrg_T>0)=1;
S_ESScrg_T=P_ESScrg_T*0;
S_ESScrg_T(P_ESScrg_T>0)=1;
S_ESSidle_T=1-S_ESScrg_T-S_ESSdcrg_T;

E_ESSstore_T_1(1)=25;
par=SUP_changed.par;
for i=2:169
    E_ESSstore_T_1(i)= E_ESSstore_T_1(i-1)*(1-par.ESS.sigma) - P_ESSdcrg_T(i-1) / (par.ESS.eta_d) * par.delta_T/60 + P_ESScrg_T(i-1)*(par.ESS.eta_c) * par.delta_T/60;  
end


SUP_changed.Base.x_res.P_ESScrg_T       =P_ESScrg_T;
SUP_changed.Base.x_res.P_ESSdcrg_T      =P_ESSdcrg_T;
SUP_changed.Base.x_res.E_ESSstore_T_1   =E_ESSstore_T_1;
SUP_changed.Base.x_res.S_ESScrg_T       =S_ESScrg_T;
SUP_changed.Base.x_res.S_ESSdcrg_T      =S_ESSdcrg_T;
SUP_changed.Base.x_res.S_ESSidle_T      =S_ESSidle_T;

dt=SUP_changed.Base.par.delta_T/60;
E=...
    sum(SUP_changed.Base.x_res.E_cons_MT)'+...
    SUP_changed.Base.x_res.P_ESScrg_T*dt-...
    SUP_changed.Base.x_res.P_ESSdcrg_T*dt-...
    SUP_changed.Base.In.E_PV_t';
    
SUP_changed.Base.x_res.E_Pur_T=E;
SUP_changed.Base.x_res.E_Pur_T(E<0)=0;
SUP_changed.Base.x_res.E_Grid_sup_T=E;
SUP_changed.Base.x_res.E_Grid_sup_T(E>0)=0;
SUP_changed.Base.x_res.S_Grid_sup_T=SUP_changed.Base.x_res.E_Grid_sup_T;
SUP_changed.Base.x_res.S_Grid_sup_T(E<0)=1;